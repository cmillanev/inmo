@extends('layouts.admin')

@section('content')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Crear Usuario</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
          <li class="breadcrumb-item active"><a href="{{ url('usuarios') }}">Usuarios</a></li>
            <li class="breadcrumb-item active">Crear usuario</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Nuevo usuario
                </div>
                <div class="card-body">
                <form action="{{ url('calculations') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" required class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" required class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" required class="form-control">
                    </div>
                    <div class="form-group">
                       <!--Crear un rol -->
                        <label for="rol">Rol</label>
                      
                        <select name="rol" id="" class="form-control">
                           
                            @foreach ($roles as $rol)

                        <option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
                                
                            @endforeach
                        </select>
                        

                   
                    </div>
                    <div class="form-group">
                        <label for="rol">Seleccione una sucursal</label>
                        <select name="idSucursal" id="" class="form-control">

                        @foreach ($offices as $office)

                    <option value="{{ $office->id  }}">{{ $office->nombre }}</option>
                            
                        @endforeach

                    </select>

                    </div>
                    <div class="box-tools pull-right ">
                        <input type="submit" value="Enviar" class="btn btn-success">
                    </div>
                </form>

                </div>

            </div>

        </div>

    </div>

</div>
@endsection