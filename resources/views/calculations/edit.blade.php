@extends('layouts.admin')


@section('content')


<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Listas de usuarios</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
            <li class="breadcrumb-item active">Solicitudes</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
    

<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12 ">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    
            
                    <div class="box-tools pull-right " >
          <a href=" {{ url('calculations/edit') }} " class="btn btn-success">Guardar </a>
                    </div>
         
                </div>
                <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <th>Id</th>
                            <th>Límite inferior $</th>
                            <th>Límite superior $</th>
                            <th>Cuota fija $</th>
                            <th>Porciento %</th>
                           
                            
                        </thead>
                        <tbody>
                           
                

                                @foreach ($datos as $dato)
                                <tr>
                                    <td>{{ $dato->id  }}</td>
                                    <td>{{ $dato->limInferior }}</td>
                                    <td>{{ $dato->limSuperior }}</td>
                                    
                                    <td>{{ $dato->cuotaFija }}</td>
                                    <td>{{ $dato->porciento }}</td>
                                    
                                    </tr>
                                @endforeach
                           
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
    </div>

</div>
@endsection
