@extends('layouts.admin')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                Editar usuario
            </div>
            <div class="card-body">
            <form action="{{ route('users.update', $users[0]->idUser ) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="form-group col-md-5">
                    <label for="name">Nombre</label>
                <input type="text" name="name" required class="form-control" value="{{ $users[0]->name}}" >
            </div>
            <div class="form-group col-md-4">
                    <label for="email">Email</label>
                    <input type="email" name="email" required class="form-control"  value="{{ $users[0]->email}}">
                </div>
                <div class="form-group col-md-3">
                    <label for="password">Password</label>
                    <input type="password" name="password"  class="form-control">
                </div>
                </div>

                <div class="row">
                 <div class="form-group col-md-4">
                    <label for="rol">Rol</label>
                    <select name="rol" id="" class="form-control">

                        <option  value="{{ $users[0]->idRol }}" >{{ $users[0]->nombre_rol }}</option>
                        @foreach ($roles as $rol)

                        <option  value="{{ $rol->id }}" >{{ $rol->nombre_rol }}</option>

                        @endforeach

                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="rol">Seleccione una sucursal</label>
                    <select name="idSucursal" id="" class="form-control">
                      <option value="{{ $users[0]->idSucursal }}">{{ $users[0]->nombre_suc }}</option>
                      @foreach ($offices as $office)
                      <option value="{{ $office->id }}">{{ $office->nombre_suc }}</option>
                      @endforeach



                </select>

            </div>
            <div class="form-group col-md-4">
                <label for="telefono">Teléfono</label>
                <input type="text" name="telefono_user"   value="{{ $users[0]->telefono_user}}" class="form-control">
            </div>
            </div>
            <div class="row">
            <div class="form-group col-md-3">
                <label for="whatsapp">WhatsApp</label>
                <input type="text" name="whatsapp"  value="{{ $users[0]->whatsapp}}" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label for="facebook">Facebook</label>
                <input type="text" name="facebook_user"  value="{{ $users[0]->facebook_user}}" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label for="linkedIn">LinkedIn</label>
                <input type="text" name="linkedIn"  value="{{ $users[0]->linkedIn}}" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label for="anio_exp">Años Experiencia</label>
                <input type="number" name="anio_exp"  value="{{ $users[0]->anio_exp}}" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="bio_user">Experiencia</label>
                <textarea class="form-control" name="bio_user" rows="3"  >{{ $users[0]->bio_user }}</textarea>
              </div>
        </div>
            </div>
                <div class="box-tools pull-right ">
                    <input type="submit" value="Enviar" class="btn btn-success">
                </div>
            </form>

            </div>

        </div>

    </div>

</div>

</div>

@endsection
