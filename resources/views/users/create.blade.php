@extends('layouts.admin')

@section('content')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Crear Usuario</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
          <li class="breadcrumb-item active"><a href="{{ url('usuarios') }}">Usuarios</a></li>
            <li class="breadcrumb-item active">Crear usuario</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Nuevo usuario
                </div>
                <div class="card-body">
                <form action="{{ url('users') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label for="name">Nombre </label>
                            <input type="text" name="name" required class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Email</label>
                            <input type="email" name="email" required class="form-control">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="password">Password</label>
                            <input type="password" name="password" required class="form-control">
                        </div>
                   </div>

                   <div class="row">
                    <div class="form-group col-md-4">
                       <!--Crear un rol -->
                        <label for="rol">Rol</label>

                        <select name="rol" id="" class="form-control">

                            @foreach ($roles as $rol)

                        <option value="{{ $rol->id }}">{{ $rol->nombre_rol }}</option>

                            @endforeach
                        </select>



                    </div>
                    <div class="form-group col-md-4">
                        <label for="rol">Seleccione una sucursal</label>
                        <select name="idSucursal" id="" required class="form-control">

                        @foreach ($offices as $office)

                    <option value="{{ $office->id  }}">{{ $office->nombre_suc }}</option>

                        @endforeach

                    </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="telefono">Teléfono</label>
                        <input type="text" name="telefono_user"  class="form-control">
                    </div>
                    </div>
                    <div class="row">
                    <div class="form-group col-md-3">
                        <label for="whatsapp">WhatsApp</label>
                        <input type="text" name="whatsapp"  class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="facebook">Facebook</label>
                        <input type="text" name="facebook_user"  class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="linkedIn">LinkedIn</label>
                        <input type="text" name="linkedIn"  class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="anio_exp">Años Experiencia</label>
                        <input type="number" name="anio_exp"  class="form-control">
                    </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleFormControlTextarea1">Experiencia</label>
                            <textarea class="form-control" name="bio_user" rows="3"></textarea>
                          </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">

                            <input type="file" class="custom-file-input" id="customFile1" name="img_user" >
                            <label class="custom-file-label" for="customFile">Seleccionar archivo</label>

                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-2 ">
                        <input type="submit" value="Enviar" class="btn btn-success">
                    </div>
                </div>
                </form>

                </div>

            </div>

        </div>

    </div>

</div>
@endsection
