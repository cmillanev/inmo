<form action="{{ route('users.destroy', $user->id )}}" method="POST" style="display:inline-block;">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-danger">Eliminar</button>
</form>