<div class="card">
    <div class="card-header" id="headingSeven">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"> <i class="fas fa-folder-plus"></i> Documentos </button>
      </h2>
    </div>
    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
      <div class="card-body">
        <p class="lead"><i class="fas fa-user-check"></i> Documentos del inquilino</p>
        <form action="{{ route('files.update', $document[0]->id) }}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          
           <input type="hidden" name="control"  value="documents">  
       
          <hr>
          <div class="row">
            <div class="col-md-6"> <br>
              <p>Identificación</p>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile" name="identificacion" >
                <label class="custom-file-label" for="customFile" >Seleccionar archivo</label>
              </div>
            </div>
            <div class="col-md-6"> <br>
              <p>Comprobante Domicilio</p>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile1" name="domicilio" >
                <label class="custom-file-label" for="customFile">Seleccionar archivo</label>
              </div>
            </div>
            <div class="col-md-6"> <br>
                <p>Comprobante Ingresos</p>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile1" name="ingresos" >
                  <label class="custom-file-label" for="customFile">Seleccionar archivo</label>
                </div>
              </div>
          </div>
          <br>
          <button class="btn btn-primary" type="submit" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><i class="fas fa-folder-plus"></i> Guardar y continuar</button>
          <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - aquí voy-->
        </form>
      </div>
    </div>
  </div>




<div class="card">
  <div class="card-header" id="headingEight">
    <h2 class="mb-0">
      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight"> <i class="fas fa-folder-plus"></i> Documentos Cliente </button>
    </h2>
  </div>
  <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
    <div class="card-body">
      <p class="lead"><i class="fas fa-user-check"></i> Documentos del inquilino</p>
      <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        
         <input type="hidden" name="control"  value="documents">  
     
        <hr>
        <div class="row">
          <div class="col-md-6"> <br>
            <p>Identificación</p>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="customFile" name="identificacion" >
              <label class="custom-file-label" for="customFile" >Seleccionar archivo</label>
            </div>
          </div>
          <div class="col-md-6"> <br>
            <p>Comprobante Domicilio</p>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="customFile1" name="domicilio" >
              <label class="custom-file-label" for="customFile">Seleccionar archivo</label>
            </div>
          </div>
          <div class="col-md-6"> <br>
              <p>Comprobante Ingresos</p>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile1" name="ingresos" >
                <label class="custom-file-label" for="customFile">Seleccionar archivo</label>
              </div>
            </div>
        </div>
        <br>
        <button class="btn btn-primary" type="submit" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><i class="fas fa-folder-plus"></i> Guardar y continuar</button>
        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - aquí voy-->
      </form>
    </div>
  </div>
</div>


