<div class="card">
    <div class="card-header" id="headingSix">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> <i class="fas fa-user-check"></i> Fiador y/o obligado solidario </button>
      </h2>
    </div>
    <div id="collapseSix" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <p class="lead"><i class="fas fa-user-check"></i> Datos del fiador</p>
        <form action="{{ route('files.update', $fiador[0]->id) }}" method="POST">
          @csrf
          @method('PUT')
          
           <input type="hidden" name="control"  value="fiadores">  
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="apellidopaterno">Apellido Paterno</label>
              <input type="text" class="form-control" id="apellidopaterno2" name="apellidoPf" value="{{ $fiador[0]->apellidoPf }}">
            </div>
            <div class="form-group col-md-6">
              <label for="apellidomaterno">Apellido Materno</label>
              <input type="text" class="form-control" id="apellidomaterno2" name="apellidoMf" value="{{ $fiador[0]->apellidoMf }}">
            </div>
          </div>
          <div class="form-group">
            <label for="nombre">Nombre (s)</label>
            <input type="text" class="form-control" id="nombre1" name="nombref" value="{{ $fiador[0]->nombref }}">
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Teléfono</label>
              <input type="tel" class="form-control"  name="telefonof" value="{{ $fiador[0]->telefonof }}">
            </div>
            <div class="form-group col-md-6">
              <label>Celular</label>
              <input type="tel" class="form-control"  name="celularf" value="{{ $fiador[0]->celularf }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Relación con el solicitante</label>
              <input type="tel" class="form-control"  name="relacionf" value="{{ $fiador[0]->relacionf }}">
            </div>
            <div class="form-group col-md-6">
              <label>Tiempo de conocerlo</label>
              <input type="tel" class="form-control"  name="tiempof" value="{{ $fiador[0]->tiempof }}">
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group col-md-2" id="nacionalidad2">
              <label>Nacionalidad:</label>
            </div>
            <div class="form-group col-md-4">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="nacionalidad2" id="mexicana1" value="option1">
                <label class="form-check-label" for="inlineRadio1">Mexicana</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="nacionalidad" id="otra1" value="option2">
                <label class="form-check-label" for="inlineRadio2">Otra</label>
              </div>
            </div>
            <div class="form-group col-md-2">
              <label for="especifique">Especifique</label>
            </div>
            <div class="form-group col-md-4">
              <input type="text" class="form-control" id="especifique1" name="especifiquef" value="{{ $fiador[0]->especifiquef }}">
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group col-md-2">Sexo:</div>
            <div class="form-group col-md-4">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="sexo3" value="option1">
                <label class="form-check-label" for="sexo1">Femenino</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="sexo4" value="option2">
                <label class="form-check-label" for="sexo2">Masculino</label>
              </div>
            </div>
            <div class="form-group col-md-2">Estado Civil:</div>
            <div class="form-group col-md-4">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="civil3" value="option1">
                <label class="form-check-label" for="civil1">Casado</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="civil4" value="option2">
                <label class="form-check-label" for="civil2">Soltero</label>
              </div>
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="mail1">E-Mail</label>
              <input type="email" class="form-control"  name="emailf" value="{{ $fiador[0]->emailf }}">
            </div>
            <div class="form-group col-md-6">
              <label for="mail2">Confirmar E-mail</label>
              <input type="email" class="form-control"  name="emailf" value="{{ $fiador[0]->emailf }}">
            </div>
          </div>
          <hr>
          <p class="lead"><i class="fas fa-id-card"></i> Identificador del fiador</p>
          <div class="form-row">
            <div class="form-group col-md-2"> Identificación </div>
            <div class="form-group col-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="id2" id="ine2" value="option1">
                <label class="form-check-label" for="ine">INE / IFE</label>
              </div>
            </div>
            <div class="form-group col-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="id2" id="pasaporte2" value="option2">
                <label class="form-check-label" for="pasaporte">Pasaporte</label>
              </div>
            </div>
            <div class="form-group col-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="id2" id="cedula2" value="option2">
                <label class="form-check-label" for="cedula">Cédula</label>
              </div>
            </div>
            <div class="form-group col-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="id2" id="licencia2" value="option2">
                <label class="form-check-label" for="licencia">Licencia</label>
              </div>
            </div>
            <div class="form-group col-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="id2" id="otroid2" value="option2">
                <label class="form-check-label" for="otroid">Otro</label>
              </div>
            </div>
          </div>
          <br>
          <div class="form-row">
            <div class="form-group col-md-6 col-lg-3">Número de identificación <br>
              <small>En caso de INE escribir <a href="#" data-toggle="tooltip" data-html="true">OCR</a> </small> </div>
            <div class="form-group col-md-6 col-lg-3">
              <input maxlength="13" type="text" class="form-control" id="noid2" name="inef" value="{{ $fiador[0]->inef }}">
            </div>
            <div class="form-group col-md-12 col-lg-2">Fecha de nacimiento</div>
            <div class="form-group col-4 col-lg-1">
              <label class="d-inline-block" for="dia">Día</label>
              <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
              </select>
            </div>
            <div class="form-group col-4 col-lg-1">
              <label class="d-inline-block" for="dia">Mes</label>
              <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
              </select>
            </div>
            <div class="form-group col-4 col-lg-2">
              <label class="d-inline-block" for="dia">Año</label>
              <select class="form-control">
                <option>2019</option>
                <option>2018</option>
                <option>2017</option>
                <option>2016</option>
                <option>2015</option>
              </select>
            </div>
          </div>
          <br>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="mail1">RFC</label>
              <input type="text" class="form-control" id="rfc2" name="rfcf" value="{{ $fiador[0]->rfcf }}">
            </div>
            <div class="form-group col-md-6">
              <label for="mail2">CURP</label>
              <input type="text" class="form-control" id="curp2" name="curpf" value="{{ $fiador[0]->curpf }}">
            </div>
          </div>
          <hr>
          <p class="lead"><i class="fas fa-home"></i> Domicilio del fiador</p>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="apellidopaterno">Calle</label>
              <input type="text" class="form-control" name="callef" value="{{ $fiador[0]->callef }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="ext">Número exterior</label>
              <input type="text" class="form-control" name="numExtf" value="{{ $fiador[0]->numExtf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="int">Número interior</label>
              <input type="text" class="form-control" name="numIntf" value="{{ $fiador[0]->numIntf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="cp">Código postal</label>
              <input type="text" class="form-control" name="cpf" value="{{ $fiador[0]->cpf }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="ext">Colonia</label>
              <input type="text" class="form-control" name="coloniaf" value="{{ $fiador[0]->coloniaf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="int">Delegación / Municipio</label>
              <input type="text" class="form-control" name="munif" value="{{ $fiador[0]->munif }}">
            </div>
            <div class="form-group col-md-4">
              <label for="cp">Estado</label>
              <input type="text" class="form-control" name="estadof" value="{{ $fiador[0]->estadof }}">
            </div>
          </div>
          <hr>
          <p class="lead"><i class="fas fa-briefcase"></i> Datos de empleo e ingresos del fiador</p>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="">Profesión oficio o puesto</label>
              <input type="text" class="form-control" name="profesionf" value="{{ $fiador[0]->profesionf }}">
            </div>
            <div class="form-group col-md-6">
              <label class="d-inline-block">Tipo de empleo</label>
              <select class="form-control">
                <option>Seleccione una opción</option>
                <option>Propietario</option>
                <option>Socio</option>
                <option>Empleado</option>
                <option>Comisionista</option>
                <option>Jubilado</option>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label>Teléfono</label>
              <input type="tel" class="form-control"  name="telefonoEf" value="{{ $fiador[0]->telefonoEf }}">
            </div>
            <div class="form-group col-md-2">
              <label>Extensión</label>
              <input type="tel" class="form-control"  name="extEf" value="{{ $fiador[0]->extEf }}">
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="apellidopaterno">Empresa donde trabaja</label>
              <input type="text" class="form-control" name="empresaf" value="{{ $fiador[0]->empresaf }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="apellidopaterno">Calle</label>
              <input type="text" class="form-control" name="calleEf" value="{{ $fiador[0]->calleEf }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="ext">Número exterior</label>
              <input type="text" class="form-control" name="nunExtEf" value="{{ $fiador[0]->nunExtEf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="int">Número interior</label>
              <input type="text" class="form-control" name="numIntEf" value="{{ $fiador[0]->numIntEf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="cp">Código postal</label>
              <input type="text" class="form-control" name="cpEf" value="{{ $fiador[0]->cpEf }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="ext">Colonia</label>
              <input type="text" class="form-control" name="coloniaEf" value="{{ $fiador[0]->coloniaEf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="int">Delegación / Municipio</label>
              <input type="text" class="form-control" name="muniEf" value="{{ $fiador[0]->muniEf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="cp">Estado</label>
              <input type="text" class="form-control" name="estadoEf" value="{{ $fiador[0]->estadoEf }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Ingreso mensual</label>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">$</div>
                </div>
                <input type="text" class="form-control"  name="ingresof" value="{{ $fiador[0]->ingresof }}">
              </div>
            </div>
            <div class="form-group col-md-12 col-lg-2">Fecha de ingreso</div>
            <div class="form-group col-4 col-lg-1">
              <label class="d-inline-block" for="dia">Día</label>
              <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
              </select>
            </div>
            <div class="form-group col-4 col-lg-1">
              <label class="d-inline-block" for="dia">Mes</label>
              <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
              </select>
            </div>
            <div class="form-group col-4 col-lg-2">
              <label class="d-inline-block" for="dia">Año</label>
              <select class="form-control">
                <option>2019</option>
                <option>2018</option>
                <option>2017</option>
                <option>2016</option>
                <option>2015</option>
              </select>
            </div>
          </div>
          <hr>
          <p class="lead"><i class="fas fa-home"></i> Datos de la propiedad del fiador</p>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="apellidopaterno">Calle</label>
              <input type="text" class="form-control" name="calleDf" value="{{ $fiador[0]->calleDf }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="ext">Número exterior</label>
              <input type="text" class="form-control" name="numExtDf" value="{{ $fiador[0]->numExtDf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="int">Número interior</label>
              <input type="text" class="form-control" name="numIntDf" value="{{ $fiador[0]->numIntDf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="cp">Código postal</label>
              <input type="text" class="form-control" name="cpDf" value="{{ $fiador[0]->cpDf }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="ext">Colonia</label>
              <input type="text" class="form-control" name="coloniaDf" value="{{ $fiador[0]->coloniaDf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="int">Delegación / Municipio</label>
              <input type="text" class="form-control" name="muniDf" value="{{ $fiador[0]->muniDf }}">
            </div>
            <div class="form-group col-md-4">
              <label for="cp">Estado</label>
              <input type="text" class="form-control" name="estadoDf" value="{{ $fiador[0]->estadoDf }}">
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Escritura número</label>
              <input type="text" class="form-control"  name="escrituraf" value="{{ $fiador[0]->escrituraf }}">
            </div>
            <div class="form-group col-md-6">
              <label>Folio real</label>
              <input type="text" class="form-control"  name="foliof" value="{{ $fiador[0]->foliof }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Notaría</label>
              <input type="text" class="form-control"  name="notariaf" value="{{ $fiador[0]->notariaf }}">
            </div>
            <div class="form-group col-md-12 col-lg-2">Fecha</div>
            <div class="form-group col-4 col-lg-1">
              <label class="d-inline-block" for="dia">Día</label>
              <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
              </select>
            </div>
            <div class="form-group col-4 col-lg-1">
              <label class="d-inline-block" for="dia">Mes</label>
              <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
              </select>
            </div>
            <div class="form-group col-4 col-lg-2">
              <label class="d-inline-block" for="dia">Año</label>
              <select class="form-control">
                <option>2019</option>
                <option>2018</option>
                <option>2017</option>
                <option>2016</option>
                <option>2015</option>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Boleta predial número</label>
              <input type="text" maxlength="21" class="form-control"  name="boletaf" value="{{ $fiador[0]->boletaf }}">
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Autorizo a investigar los datos por cualquier medio legal, incluyendo buró de crédito.</label>
            </div>
            <hr>
          </div>
          <div class="form-row">
            <div class="form-group form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck12">
              <label class="form-check-label" for="exampleCheck1">Declaro bajo portesta de decir verdad que los datos asentados son correctos y acepto que será causa de rescisión del contrato de arrendamiento la falsedad de cualquiera de ellos.</label>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6"> <br>
              <p>Firma del solicitante</p>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Seleccionar archivo</label>
              </div>
            </div>
            <div class="col-md-6"> <br>
              <p>Firma de obligado solidario y/o fiador</p>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile1">
                <label class="custom-file-label" for="customFile">Seleccionar archivo</label>
              </div>
            </div>
          </div>
          <br>
          <button class="btn btn-primary" type="submit" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><i class="fas fa-check"></i> Guardar y continuar</button>
          <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - aquí voy-->
        </form>
      </div>
    </div>
  </div>