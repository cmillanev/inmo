<div class="card">
    <div class="card-header" id="headingOne">
        <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne"
                aria-expanded="false" aria-controls="collapseOne"><i class="fas fa-user"></i> Datos Personales </button>
        </h2>
    </div>
    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body">
            <p class="lead"><i class="fas fa-user"></i> Datos Personales</p>
            <form action="{{ route('files.update', $personals[0]->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="apellidoP">Apellido Paterno</label>
                        <input type="text" class="form-control" name="apellidoP"
                            value="{{ $personals[0]->apellidoP }}" placeholder="">
                        <input type="hidden" name="control" value="personals">
                        <input type="hidden" name="status" value="2">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="apellidoM">Apellido Materno</label>
                        <input type="text" class="form-control" name="apellidoM"
                            value="{{ $personals[0]->apellidoM }}" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nombres">Nombre (s)</label>
                    <input type="text" class="form-control" name="nombres" value="{{ $personals[0]->nombres }}"
                        placeholder="">
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-2" id="nacionalidad">
                        <label>Nacionalidad:</label>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="form-check form-check-inline">

                            @if($personals[0]->nacionalidad  == '0')
                                <input class="form-check-input" type="radio" name="nacionalidad" id="mexicana" checked
                                    value="0">
                                <label class="form-check-label" for="inlineRadio1">Mexicana</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="nacionalidad" id="otra" value="1">
                            <label class="form-check-label" for="inlineRadio2">Otra</label>
                        </div>

                    @elseif($personals[0]->nacionalidad  == '1')
                        <input class="form-check-input" type="radio" name="nacionalidad" id="mexicana" value="0">
                        <label class="form-check-label" for="inlineRadio1">Mexicana</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="nacionalidad" id="otra" checked value="1">
                        <label class="form-check-label" for="inlineRadio2">Otra</label>
                    </div>
                @else
                    <input class="form-check-input" type="radio" name="nacionalidad" id="mexicana" value="0">
                    <label class="form-check-label" for="inlineRadio1">Mexicana</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="nacionalidad" id="otra" value="1">
                    <label class="form-check-label" for="inlineRadio2">Otra</label>
                </div>

                @endif


        </div>
        <div class="form-group col-md-2">
            <label for="especifiqueN">Especifique</label>
        </div>
        <div class="form-group col-md-4">
            <input type="text" class="form-control" name="especifiqueN" value="{{ $personals[0]->especifiqueN }}">
        </div>
    </div>
    <hr>
    <div class="form-row">
        <div class="form-group col-md-2">Sexo:</div>
        <div class="form-group col-md-4">
            @if($personals[0]->sexo == '0')
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="sexo" id="sexo1" checked value="0">
                    <label class="form-check-label" for="sexo1">Femenino</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="sexo" id="sexo2" value="1">
                    <label class="form-check-label" for="sexo2">Masculino</label>
                </div>
            @elseif($personals[0]->sexo == '1')

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="sexo" id="sexo1" value="0">
                    <label class="form-check-label" for="sexo1">Femenino</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="sexo" id="sexo2" checked value="1">
                    <label class="form-check-label" for="sexo2">Masculino</label>
                </div>

            @else

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="sexo" id="sexo1" value="0">
                    <label class="form-check-label" for="sexo1">Femenino</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="sexo" id="sexo2" value="1">
                    <label class="form-check-label" for="sexo2">Masculino</label>
                </div>

            @endif
        </div>
        <div class="form-group col-md-2">Estado Civil:</div>
        <div class="form-group col-md-4">
            @if($personals[0]->sexo == '0')

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="edoCivil" checked id="civil1" value="0">
                    <label class="form-check-label" for="civil1">Casado</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="edoCivil" id="civil2" value="1">
                    <label class="form-check-label" for="civil2">Soltero</label>
                </div>

            @elseif($personals[0]->sexo == '1')

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="edoCivil" id="civil1" value="0">
                    <label class="form-check-label" for="civil1">Casado</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="edoCivil" checked id="civil2" value="1">
                    <label class="form-check-label" for="civil2">Soltero</label>
                </div>
            @else

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="edoCivil" id="civil1" value="0">
                    <label class="form-check-label" for="civil1">Casado</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="edoCivil" id="civil2" value="1">
                    <label class="form-check-label" for="civil2">Soltero</label>
                </div>

            @endif
        </div>
    </div>
    <hr>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="mail1">E-Mail</label>
            <input type="email" class="form-control" name="email" placeholder="{{ $personals[0]->email }}">
        </div>
        <div class="form-group col-md-6">
            <label for="mail2">Confirmar E-mail</label>
            <input type="email" class="form-control" name="email" value="{{ $personals[0]->email }}">
        </div>
    </div>
    <hr>
    <div class="form-row">
        <div class="form-group col-md-2"> Identificación </div>
        @if($personals[0]->iden == '0')
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="ine" checked value="0">
                    <label class="form-check-label" for="ine">INE / IFE</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="pasaporte" value="1">
                    <label class="form-check-label" for="pasaporte">Pasaporte</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="cedula" value="2">
                    <label class="form-check-label" for="cedula">Cédula</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="licencia" value="3">
                    <label class="form-check-label" for="licencia">Licencia</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="otroid" value="4">
                    <label class="form-check-label" for="otroid">Otro</label>
                </div>
            </div>
        @elseif($personals[0]->iden == '1')

            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="ine" value="0">
                    <label class="form-check-label" for="ine">INE / IFE</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="pasaporte" checked value="1">
                    <label class="form-check-label" for="pasaporte">Pasaporte</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="cedula" value="2">
                    <label class="form-check-label" for="cedula">Cédula</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="licencia" value="3">
                    <label class="form-check-label" for="licencia">Licencia</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="otroid" value="4">
                    <label class="form-check-label" for="otroid">Otro</label>
                </div>
            </div>
        @elseif($personals[0]->iden == '2')

            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="ine" value="0">
                    <label class="form-check-label" for="ine">INE / IFE</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="pasaporte" value="1">
                    <label class="form-check-label" for="pasaporte">Pasaporte</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="cedula" checked value="2">
                    <label class="form-check-label" for="cedula">Cédula</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="licencia" value="3">
                    <label class="form-check-label" for="licencia">Licencia</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="otroid" value="4">
                    <label class="form-check-label" for="otroid">Otro</label>
                </div>
            </div>
        @elseif(($personals[0]->iden == '3'))
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="ine" value="0">
                    <label class="form-check-label" for="ine">INE / IFE</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="pasaporte" value="1">
                    <label class="form-check-label" for="pasaporte">Pasaporte</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="cedula" value="2">
                    <label class="form-check-label" for="cedula">Cédula</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="licencia" checked value="3">
                    <label class="form-check-label" for="licencia">Licencia</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="otroid" value="4">
                    <label class="form-check-label" for="otroid">Otro</label>
                </div>
            </div>
        @elseif($personals[0]->iden == '4')
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="ine" value="0">
                    <label class="form-check-label" for="ine">INE / IFE</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="pasaporte" value="1">
                    <label class="form-check-label" for="pasaporte">Pasaporte</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="cedula" value="2">
                    <label class="form-check-label" for="cedula">Cédula</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="licencia" value="3">
                    <label class="form-check-label" for="licencia">Licencia</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="otroid" checked value="4">
                    <label class="form-check-label" for="otroid">Otro</label>
                </div>
            </div>
        @else
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="ine" value="0">
                    <label class="form-check-label" for="ine">INE / IFE</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="pasaporte" value="1">
                    <label class="form-check-label" for="pasaporte">Pasaporte</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="cedula" value="2">
                    <label class="form-check-label" for="cedula">Cédula</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="licencia" value="3">
                    <label class="form-check-label" for="licencia">Licencia</label>
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="iden" id="otroid" value="4">
                    <label class="form-check-label" for="otroid">Otro</label>
                </div>
            </div>
        @endif

    </div>
    <br>
    <div class="form-row">
        <div class="form-group col-md-6 col-lg-3">Número de identificación <br>
            <small>En caso de INE escribir <a href="#" data-toggle="tooltip" data-html="true"
                    title="Ejemplo número de identificación<br><img src=&quot;https://via.placeholder.com/350x250&quot;>">OCR</a>
            </small> </div>
        <div class="form-group col-md-6 col-lg-3">
            <input maxlength="13" type="text" class="form-control" name="ine" id="noid"
                value="{{ $personals[0]->ine }}">
        </div>
        <div class="form-group col-md-12 col-lg-2">Fecha de nacimiento</div>
        <div class="form-group col-4 col-lg-1">
            <label class="d-inline-block" for="dia">Día</label>
            <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
            </select>
        </div>
        <div class="form-group col-4 col-lg-1">
            <label class="d-inline-block" for="dia">Mes</label>
            <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
            </select>
        </div>
        <div class="form-group col-4 col-lg-2">
            <label class="d-inline-block" for="dia">Año</label>
            <select class="form-control">
                <option>2019</option>
                <option>2018</option>
                <option>2017</option>
                <option>2016</option>
                <option>2015</option>
            </select>
        </div>
    </div>
    <br>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="rfc">RFC</label>
            <input type="text" class="form-control" name="rfc" id="rfc" value="{{ $personals[0]->rfc }}">
        </div>
        <div class="form-group col-md-6">
            <label for="curp">CURP</label>
            <input type="text" class="form-control" name="curp" id="curp" value="{{ $personals[0]->curp }}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="telefonoP">Teléfono fijo</label>
            <input type="text" class="form-control" name="telefonoP" value="{{ $personals[0]->telefonoP }}">
        </div>
        <div class="form-group col-md-6">
            <label for="celular">Teléfono celular</label>
            <input type="text" class="form-control" name="celular" value="{{ $personals[0]->celular }}"
                placeholder="">
        </div>
    </div>
    <hr>
    <p class="lead"><i class="fas fa-user-friends"></i> Datos del cónyuge</p>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="apellidoPc">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidoPc" value="{{ $personals[0]->apellidoPc }}">
        </div>
        <div class="form-group col-md-6">
            <label for="apellidoMc">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidoMc" value="{{ $personals[0]->apellidoMc }}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="nombrec">Nombre (s)</label>
            <input type="text" class="form-control" name="nombrec" value="{{ $personals[0]->nombrec }}">
        </div>
        <div class="form-group col-md-6">
            <label for="telefonoc">Teléfono</label>
            <input type="tel" class="form-control" name="telefonoc" value="{{ $personals[0]->telefonoc }}">
        </div>
    </div>
    <hr>
    <div class="form-row">
        <div class="form-group col-8 col-sm-auto">Adultos que ocuparán el inmueble </div>
        <div class="form-group col-4 col-md-1">
            <input type="text" class="form-control" name="adultos" value="{{ $personals[0]->adultos }}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-8 col-sm-auto">Menores de 18 años</div>
        <div class="form-group col-4 col-md-4">

            @if($personals[0]->menores == 0)
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="menores" checked value="0">
                    <label class="form-check-label" for="menores">Si</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="menores" value="1">
                    <label class="form-check-label" for="menores">No</label>
                </div>
            @elseif($personals[0]->menores == 1)

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="menores" value="0">
                    <label class="form-check-label" for="menores">Si</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="menores" checked value="1">
                    <label class="form-check-label" for="menores">No</label>
                </div>
            @else

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="menores" value="0">
                    <label class="form-check-label" for="menores">Si</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="menores" value="1">
                    <label class="form-check-label" for="menores">No</label>
                </div>
            @endif


        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-8 col-sm-auto">Cuantos </div>
        <div class="form-group col-4 col-md-1">
            <input type="text" class="form-control" name="cuantos" value="{{ $personals[0]->cuantos }}">
        </div>
    </div>
    <hr>
    <p class="lead"><i class="fas fa-home"></i> Domicilio</p>
    <div class="form-row">
        <div class="form-group col-12 col-md-auto"> Situación habitacional </div>
        <div class="form-group col-12 col-md-4">
            <select class="form-control" name="situacion">
                <option value="">Seleccione una opción</option>
                <option value="Inquilino">Inquilino</option>
                <option value="Pensión - Hotel">Pensión - Hotel</option>
                <option value="Con padres o familiares">Con padres o familiares</option>
                <option value="Propietario pagando">Propietario pagando</option>
                <option value="Propietario liberado">Propietario liberado</option>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-auto">
            <label for="apellidopaterno">Motivo por el cual se cambia</label>
        </div>
        <div class="form-group col-md-6">
            <textarea class="form-control" name="motivo" rows="3" value="{{ $personals[0]->motivo }}"></textarea>
        </div>
    </div>
    <hr>
    <p class="lead"><i class="fas fa-dog"></i> Mascotas</p>
    <div class="form-row">
        <div class="form-group col-md-2">
            <label>Mascotas:</label>
        </div>
        <div class="form-group col-md-4">

            @if($personals[0]->mascotas  == 1)
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="mascotas" checked value="1">
                    <label class="form-check-label" for="inlineRadio1">Si</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="mascotas" value="0">
                    <label class="form-check-label" for="inlineRadio2">No</label>
                </div>
            @elseif($personals[0]->mascotas  == 0)
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="mascotas" value="1">
                    <label class="form-check-label" for="inlineRadio1">Si</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="mascotas" checked value="0">
                    <label class="form-check-label" for="inlineRadio2">No</label>
                </div>
            @else
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="mascotas" value="1">
                    <label class="form-check-label" for="inlineRadio1">Si</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="mascotas" value="0">
                    <label class="form-check-label" for="inlineRadio2">No</label>
                </div>
            @endif

        </div>
        <div class="form-group col-md-2">
            <label for="especifique">Especifique</label>
        </div>
        <div class="form-group col-md-4">
            <input type="text" class="form-control" name="especifique" id="mascota"
                value="{{ $personals[0]->especifique }}">
        </div>
    </div>
    <hr>
    <p class="lead"><i class="fas fa-home"></i> Domicilio donde vive actualmente</p>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="calle">Calle</label>
            <input type="text" class="form-control" name="calle" value="{{ $personals[0]->calle }}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="numExt">Número exterior</label>
            <input type="text" class="form-control" name="numExt" value="{{ $personals[0]->numExt }}">
        </div>
        <div class="form-group col-md-4">
            <label for="numInt">Número interior</label>
            <input type="text" class="form-control" name="numInt" value="{{ $personals[0]->numInt }}">
        </div>
        <div class="form-group col-md-4">
            <label for="cp">Código postal</label>
            <input type="text" class="form-control" name="cp" value="{{ $personals[0]->cp }}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="colonia">Colonia</label>
            <input type="text" class="form-control" name="colonia" value="{{ $personals[0]->colonia }}">
        </div>
        <div class="form-group col-md-4">
            <label for="mun">Delegación / Municipio</label>
            <input type="text" class="form-control" name="mun" value="{{ $personals[0]->mun }}">
        </div>
        <div class="form-group col-md-4">
            <label for="estado">Estado</label>
            <input type="text" class="form-control" name="estado" value="{{ $personals[0]->estado }}">
        </div>
    </div>
    <hr>
    <p class="lead"><i class="fas fa-user"></i> Nombre del arrendador actual</p>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="apellidoPa">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidoPa" value="{{ $personals[0]->apellidoPa }}">
        </div>
        <div class="form-group col-md-6">
            <label for="apellidoMa">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidoMa" value="{{ $personals[0]->apellidoMa }}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="nombrea">Nombre (s)</label>
            <input type="text" class="form-control" name="nombrea" value="{{ $personals[0]->nombrea }}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="P">Teléfono</label>
            <input type="tel" class="form-control" name="telefonoa" value="{{ $personals[0]->telefonoa }}">
        </div>
        <div class="form-group col-md-4">
            <label for="renta">Renta que paga actualmente</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">$</div>
                </div>
                <input type="text" class="form-control" name="renta" value="{{ $personals[0]->renta }}">
            </div>
        </div>
        <div class="form-group col-md-4">
            <label for="anioRenta">Ocupa el lugar desde (año)</label>
            <input type="text" class="form-control" name="anioRenta" value="{{ $personals[0]->anioRenta }}">
        </div>
    </div>
    <hr>
    <div class="accordion" id="accordionTwo">
        <div class="card">
            <div class="card-header" id="headingFour">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour"
                        aria-expanded="false" aria-controls="collapseFour"> <i class="fas fa-building"></i> Solicitante
                        persona moral </button>
                </h2>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionTwo">
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="nombreEmp">Nombre de la empresa o razón social</label>
                            <input type="text" class="form-control" name="nombreEmp"
                                value="{{ $personals[0]->nombreEmp }}">
                        </div>
                    </div>
                    <hr>
                    <p class="lead"><i class="fas fa-user-tie"></i> Apoderado legal y/o representante legal</p>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="apellidoPe">Apellido Paterno</label>
                            <input type="text" class="form-control" name="apellidoPe"
                                value="{{ $personals[0]->apellidoPe }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="apellidoMe">Apellido Materno</label>
                            <input type="text" class="form-control" name="apellidoMe"
                                value="{{ $personals[0]->apellidoMe }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="nombreRazon">Nombre (s)</label>
                            <input type="text" class="form-control" name="nombreRazon"
                                value="{{ $personals[0]->nombreRazon }}">
                        </div>
                    </div>
                    <hr>
                    <p class="lead"><i class="far fa-personals"></i> Acta constitutiva de la empresa</p>
                    <div class="form-row">
                        <div class="form-group col-md-12 col-lg-3">
                            <label>Escritura pública número</label>
                            <input type="text" class="form-control" name="escritura"
                                value="{{ $personals[0]->escritura }}">
                        </div>
                        <div class="form-group col-md-12 col-lg-3">
                            <label>Notario número</label>
                            <input type="text" class="form-control" name="notario"
                                value="{{ $personals[0]->notario }}">
                        </div>
                        <div class="form-group col-md-12 col-lg-2">
                            <label>Fecha de constitución</label>
                        </div>
                        <div class="form-group col-4 col-lg-1">
                            <label class="d-inline-block" for="dia">Día</label>
                            <select class="form-control">
                                <option>01</option>
                                <option>02</option>
                                <option>03</option>
                                <option>04</option>
                                <option>05</option>
                            </select>
                        </div>
                        <div class="form-group col-4 col-lg-1">
                            <label class="d-inline-block" for="dia">Mes</label>
                            <select class="form-control">
                                <option>01</option>
                                <option>02</option>
                                <option>03</option>
                                <option>04</option>
                                <option>05</option>
                            </select>
                        </div>
                        <div class="form-group col-4 col-lg-2">
                            <label class="d-inline-block" for="dia">Año</label>
                            <select class="form-control">
                                <option>2019</option>
                                <option>2018</option>
                                <option>2017</option>
                                <option>2016</option>
                                <option>2015</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Giro comercial</label>
                            <input type="Text" class="form-control" name="giro" value="{{ $personals[0]->giro }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Teléfono</label>
                            <input type="tel" class="form-control" name="telefonoE"
                                value="{{ $personals[0]->telefonoE }}">
                        </div>
                        <div class="form-group col-md-2">
                            <label>Extensión</label>
                            <input type="tel" class="form-control" name="extE" value="{{ $personals[0]->extE }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>E-mail</label>
                            <input type="mail" class="form-control" name="emailE"
                                value="{{ $personals[0]->emailE }}">
                        </div>
                        <div class="form-group col-md-6"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingFive">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> <i
                            class="fas fa-personals-invoice"></i> Datos fiscales </button>
                </h2>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionTwo">
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Nombre o razón social</label>
                            <input type="text" class="form-control" name="nombreF"
                                value="{{ $personals[0]->nombreF }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Correo electónico para facturación</label>
                            <input type="mail" class="form-control" name="correoF"
                                value="{{ $personals[0]->correoF }}">
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            @if($personals[0]->dirFisc == 1)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="dirFisc" id="inlineCheckbox1"
                                        checked value="1">
                                    <label class="form-check-label" for="inlineCheckbox1">Dirección fiscal </label>
                                </div>
                            @else
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="dirFisc" id="inlineCheckbox1"
                                        value="1">
                                    <label class="form-check-label" for="inlineCheckbox1">Dirección fiscal </label>
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-9">
                            <label>RFC</label>
                            <input type="text" class="form-control" name="rfcfis"
                                value="{{ $personals[0]->rfcfis }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="apellidopaterno">Calle</label>
                            <input type="text" class="form-control" name="calleF"
                                value="{{ $personals[0]->calleF }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="numExtF">Número exterior</label>
                            <input type="text" class="form-control" name="numExtF"
                                value="{{ $personals[0]->numExtF }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="numIntF">Número interior</label>
                            <input type="text" class="form-control" name="numIntF"
                                value="{{ $personals[0]->numIntF }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cpF">Código postal</label>
                            <input type="text" class="form-control" name="cpF" value="{{ $personals[0]->cpF }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="coloniaF">Colonia</label>
                            <input type="text" class="form-control" name="coloniaF"
                                value="{{ $personals[0]->coloniaF }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="munF">Delegación / Municipio</label>
                            <input type="text" class="form-control" name="munF" value="{{ $personals[0]->munF }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="estadoF">Estado</label>
                            <input type="text" class="form-control" name="estadoF"
                                value="{{ $personals[0]->estadoF }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <button class="btn btn-primary" type="submit" data-toggle="collapse" data-target="#collapseTwo"
        aria-expanded="false" aria-controls="collapseTwo"><i class="fas fa-check"></i> Guardar y continuar</button>
    <br>
    <br>
    <div class="alert alert-danger" role="alert">
        <i class="far fa-times-circle"></i> Lo sentimos, algunos campos contienen errores o están incompletos.
    </div>
    </form>
</div>
</div>
</div>