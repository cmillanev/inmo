<div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <i class="fas fa-briefcase"></i> Datos de empleo e ingresos </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <form action="{{ route('files.update', $jobs[0]->id) }}" method="POST">
          @csrf
          @method('PUT')
          <p class="lead"><i class="fas fa-briefcase"></i> Datos de empleo</p>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="profesion">Profesión oficio o puesto</label>
            <input type="text" class="form-control" name="profesion" value="{{ $jobs[0]->profesion }}" >
          <input type="hidden" name="control"  value="jobs">  
          </div>
            <div class="form-group col-md-6">
              <label class="d-inline-block">Tipo de empleo</label>
              <select class="form-control">
                <option>Seleccione una opción</option>
                <option>Propietario</option>
                <option>Socio</option>
                <option>Empleado</option>
                <option>Comisionista</option>
                <option>Jubilado</option>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label>Teléfono</label>
            <input type="tel" class="form-control" name="tel" value="{{ $jobs[0]->tel }}">
            </div>
            <div class="form-group col-md-2">
              <label>Extensión</label>
            <input type="tel" class="form-control"  name="ext" value="{{ $jobs[0]->ext }}">
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="empresa">Empresa donde trabaja</label>
            <input type="text" class="form-control" name="empresa" value="{{ $jobs[0]->empresa }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="calle">Calle</label>
            <input type="text" class="form-control" name="calle" value="{{ $jobs[0]->calle }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="ext">Número exterior</label>
              <input type="text" class="form-control" name="numExt" value="{{ $jobs[0]->numExt }}">
            </div>
            <div class="form-group col-md-4">
              <label for="int">Número interior</label>
              <input type="text" class="form-control" name="numInt" value="{{ $jobs[0]->numInt }}">
            </div>
            <div class="form-group col-md-4">
              <label for="cp">Código postal</label>
              <input type="text" class="form-control" name="cp" value="{{ $jobs[0]->cp }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="ext">Colonia</label>
              <input type="text" class="form-control" name="colonia" value="{{ $jobs[0]->colonia }}">
            </div>
            <div class="form-group col-md-4">
              <label for="int">Delegación / Municipio</label>
              <input type="text" class="form-control" name="mun" value="{{ $jobs[0]->mun }}">
            </div>
            <div class="form-group col-md-4">
              <label for="cp">Estado</label>
              <input type="text" class="form-control" name="estado" value="{{ $jobs[0]->estado }}">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-12 col-lg-2">Fecha de ingreso</div>
            <div class="form-group col-4 col-lg-1">
              <label class="d-inline-block" for="dia">Día</label>
              <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
              </select>
            </div>
            <div class="form-group col-4 col-lg-1">
              <label class="d-inline-block" for="dia">Mes</label>
              <select class="form-control">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
              </select>
            </div>
            <div class="form-group col-4 col-lg-2">
              <label class="d-inline-block" for="dia">Año</label>
              <select class="form-control">
                <option>2019</option>
                <option>2018</option>
                <option>2017</option>
                <option>2016</option>
                <option>2015</option>
              </select>
            </div>
          </div>
          <hr>
          <p class="lead"><i class="fas fa-user-tie"></i> Jefe inmediato</p>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="">Apellido Paterno</label>
              <input type="text" class="form-control"  name="apellidoPj" value="{{ $jobs[0]->apellidoPj }}">
            </div>
            <div class="form-group col-md-6">
              <label for="">Apellido Materno</label>
              <input type="text" class="form-control"  name="apellidoMj" value="{{ $jobs[0]->apellidoMj }}">
            </div>
          </div>
          <div class="form-group">
            <label for="">Nombre (s)</label>
            <input type="text" class="form-control"  name="nombrej" value="{{ $jobs[0]->nombrej }}">
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label>Teléfono de oficina</label>
              <input type="tel" class="form-control"  name="telefonoj" value="{{ $jobs[0]->telefonoj }}">
            </div>
            <div class="form-group col-md-2">
              <label>Extensión</label>
              <input type="tel" class="form-control"  name="extj" value="{{ $jobs[0]->extj }}">
            </div>
          </div>
          <hr>
          <p class="lead"><i class="fas fa-dollar-sign"></i> Ingresos</p>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Ingreso mensual comprobable</label>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">$</div>
                </div>
                <input type="text" class="form-control"  name="ingresos" value="{{ $jobs[0]->ingresos }}">
              </div>
            </div>
            <div class="form-group col-md-6">
              <label>Ingreso familiar mensual comprobable</label>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">$</div>
                </div>
                <input type="text" class="form-control"  name="ingresoFam" value="{{ $jobs[0]->ingresoFam }}">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="col-md-6">
              <label>Número de personas que aportan al ingreso familiar</label>
              <input type="text" class="form-control col-2"  name="numPerIngre" value="{{ $jobs[0]->numPerIngre }}">
            </div>
            <div class="col-md-6">
              <label>Número de personas que dependen de usted</label>
              <input type="text" class="form-control col-2"  name="numPerDep" value="{{ $jobs[0]->numPerDep }}">
            </div>
          </div>
          <br>
          <div class="card">
            <div class="card-header"> <i class="fas fa-exclamation-circle"></i> Llenar sólo en caso de que alguna persona más aporte al ingreso familiar </div>
            <div class="card-body">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="">Apellido Paterno</label>
                  <input type="text" class="form-control"  name="apellidoPa" value="{{ $jobs[0]->apellidoPa }}">
                </div>
                <div class="form-group col-md-6">
                  <label for="">Apellido Materno</label>
                  <input type="text" class="form-control"  name="apellidoMa" value="{{ $jobs[0]->apellidoMa }}">
                </div>
              </div>
              <div class="form-group">
                <label for="">Nombre (s)</label>
                <input type="text" class="form-control"  name="nombrea" value="{{ $jobs[0]->nombrea }}">
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="">Parentesco</label>
                  <input type="text" class="form-control"  name="parentesco" value="{{ $jobs[0]->parentesco }}">
                </div>
                <div class="form-group col-md-6">
                  <label for="">Teléfono</label>
                  <input type="tel" class="form-control"  name="telefonoA" value="{{ $jobs[0]->telefonoA }}">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="">Empresa donde trabaja</label>
                  <input type="text" class="form-control"  name="empresaA" value="{{ $jobs[0]->empresaA }}">
                </div>
                <div class="form-group col-md-6">
                  <label>Ingreso mensual comprobable</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">$</div>
                    </div>
                    <input type="text" class="form-control"  name="ingresoA" value="{{ $jobs[0]->ingresoA }}">
                  </div>
                </div>
              </div>
            </div>
          </div>
       
        <br>
        <button class="btn btn-primary" type="sumbmit" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="fas fa-check"></i> Guardar y continuar</button>
      </div>
    </form>
    </div>
  </div>