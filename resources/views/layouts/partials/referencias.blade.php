<div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <i class="fas fa-check-square"></i> Referencias </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <form action="{{ route('files.update', $ref[0]->id) }}" method="POST">
          @csrf
          @method('PUT')
          <input type="hidden" name="control"  value="references">  
        <p class="lead"><i class="fas fa-user-friends"></i> Referencias personales</p>
        <p><i class="fas fa-user-check"></i> <small class="text-muted">Referencia personal 1</small></p>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidoPr1" value="{{ $ref[0]->apellidoPr1 }}">
          </div>
          <div class="form-group col-md-6">
            <label for="">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidoMr1" value="{{ $ref[0]->apellidoMr1 }}">
          </div>
        </div>
        <div class="form-group">
          <label for="">Nombre (s)</label>
          <input type="text" class="form-control" name="nombrer1" value="{{ $ref[0]->nombrer1 }}">
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Relación</label>
            <input type="text" class="form-control" name="relacionr1" value="{{ $ref[0]->relacionr1 }}">
          </div>
          <div class="form-group col-md-6">
            <label>Teléfono</label>
            <input type="tel" class="form-control" name="telefonor1" value="{{ $ref[0]->telefonor1 }}">
          </div>
        </div>
        <hr>
        <p><i class="fas fa-user-check"></i> <small class="text-muted">Referencia personal 2</small></p>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidoPr2" value="{{ $ref[0]->apellidoPr2 }}">
          </div>
          <div class="form-group col-md-6">
            <label for="">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidoMr2" value="{{ $ref[0]->apellidoMr2 }}">
          </div>
        </div>
        <div class="form-group">
          <label for="">Nombre (s)</label>
          <input type="text" class="form-control" name="nombrer2" value="{{ $ref[0]->nombrer2 }}">
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Relación</label>
            <input type="text" class="form-control" name="relacionr2" value="{{ $ref[0]->relacionr2 }}">
          </div>
          <div class="form-group col-md-6">
            <label>Teléfono</label>
            <input type="tel" class="form-control" name="telefonor2" value="{{ $ref[0]->telefonor2 }}">
          </div>
        </div>
        <hr>
        <p class="lead"><i class="fas fa-user-friends"></i> Referencias familiares</p>
        <p><i class="fas fa-user-check"></i> <small class="text-muted">Referencia familiar 1</small></p>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidoPf1" value="{{ $ref[0]->apellidoPf1 }}">
          </div>
          <div class="form-group col-md-6">
            <label for="">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidoMf1" value="{{ $ref[0]->apellidoMf1 }}">
          </div>
        </div>
        <div class="form-group">
          <label for="">Nombre (s)</label>
          <input type="text" class="form-control" name="nombref1" value="{{ $ref[0]->nombref1 }}">
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Relación</label>
            <input type="text" class="form-control" name="relacionf1" value="{{ $ref[0]->relacionf1 }}">
          </div>
          <div class="form-group col-md-6">
            <label>Teléfono</label>
            <input type="tel" class="form-control" name="telefonof1" value="{{ $ref[0]->telefonof1 }}">
          </div>
        </div>
        <hr>
        <p><i class="fas fa-user-check"></i> <small class="text-muted">Referencia familiar 2</small></p>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidoPf2" value="{{ $ref[0]->apellidoPf2 }}">
          </div>
          <div class="form-group col-md-6">
            <label for="">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidoMf2" value="{{ $ref[0]->apellidoMf2 }}">
          </div>
        </div>
        <div class="form-group">
          <label for="">Nombre (s)</label>
          <input type="text" class="form-control" name="nombref2" value="{{ $ref[0]->nombref2 }}">
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Relación</label>
            <input type="text" class="form-control" name="relacionf2" value="{{ $ref[0]->relacionf2 }}">
          </div>
          <div class="form-group col-md-6">
            <label>Teléfono</label>
            <input type="tel" class="form-control" name="telefonof2" value="{{ $ref[0]->telefonof2 }}">
          </div>
        </div>
        <button class="btn btn-primary" type="submit" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><i class="fas fa-check"></i> Guardar y continuar</button>
      </form>
      </div>
    </div>
  </div>