@extends('layouts.admin')



@section('content')


<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ url('solicitudes') }}">Solicitudes</a></li>
            <li class="breadcrumb-item active">Nueva</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

  <div class="container">
    <div class="row content-padding-sm">
      <div class="col-lg-12">
        <h2>Tol01000001</h2>
      </div>
    </div>
    <div class="row content-padding-bottom-sm">
      <div class="col-lg-12">
        <div class="accordion" id="accordionExample">
        
            @include('layouts.partials.personales')
        
            @include('layouts.partials.empleo')  
            
            @include('layouts.partials.referencias')

            @include('layouts.partials.fiador')

            @include('layouts.partials.docs')

            </div>
        </div>
      </div>
       <div class="row justify-content-center">
        
        <form action="{{ route('files.update', $personals[0]->id) }}" method="POST">
          @csrf
          @method('PUT')
          
           <input type="hidden" name="control"  value="terminado">  
           <input type="hidden" name="status" value="3">
        <div class="col-3">
          <button class="btn btn-primary btn-lg " type="submit"><i class="fas fa-check-circle"></i> Enviar solicitud</button>
          <br>
          <br>
        </div>
      </form>
      </div>
    </div>
                               
                                      
                                   
        
        
    <script>
        $('.collapse').on('shown.bs.collapse', function(e) {
          var $card = $(this).closest('.card');
          $('html,body').animate({
            scrollTop: $card.offset().top
          }, 500);
        });
        </script> 
        <!--input file--> 
        <script>
        $(".custom-file-input").on("change", function() {
          var fileName = $(this).val().split("\\").pop();
          $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
        </script> 
        <!--tooltip--> 
        <script>
            $( document ).ready(function() {
                $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
            });
            </script> 
        
        
        @stop
