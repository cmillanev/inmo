@extends('layouts.admin')


@section('content')

<div class="col-lg-12">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Información de póliza</h3>
        </div>
        <!-- /.card-header -->
     
      </div>
    </div>

    
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Información</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#documentos" role="tab" aria-controls="documentos" aria-selected="false">Documentos</a>
      </li>
      @if ($file[0]->status == 3  )

      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Análisis</a>
      </li>
    
      @endif  
      
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

          <!-- form start -->
          <form role="form">
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="folio">Folio</label>
                        <input type="text" class="form-control" id="folio" value="{{ $file[0]->clave }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="nombre">Nombre</label>
                        <input type="email" class="form-control" id="nombres"
                            value="{{ $file[0]->nombres }} {{ $file[0]->apellidoP }} {{ $file[0]->apellidoM }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="celular">Celular</label>
                        <input type="text" class="form-control" id="celular" value="{{ $file[0]->celular }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="estatus">Estatus</label>
                        @switch($file[0]->status)
                        @case(1)
                         
                        <input type="text" class="form-control" id="estatus" value="Nueva">
                           
                            @break
                        @case(2)
                        <input type="text" class="form-control" id="estatus" value="Documentos">
                            @break
                        @case(3)
                        <input type="text" class="form-control" id="estatus" value="Análisis">
                        @default

                    @endswitch
                        
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="renta">Renta</label>
                        <input type="text" class="form-control" id="reanta" value="{{ $file[0]->renta }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="montoPoliza">Monto Póliza</label>
                        <input type="text" class="form-control" value="{{ $file[0]->montoPoliza }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="calle">Calle</label>
                        <input type="text" class="form-control" name="calle" value="{{ $file[0]->calle }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="calle">Número Ext</label>
                        <input type="text" class="form-control" name="numero" value="{{ $file[0]->numExt }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="calle">Número Int</label>
                        <input type="text" class="form-control" name="numero" value="{{ $file[0]->numInt }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="referenciasUnicacion">Referencias Ubicación</label>
                        <input type="text" class="form-control" name="folio"
                            value="{{ $file[0]->referenciasUbicacion }}"">
              </div>
              <div class=" form-group col-md-3">
                        <label for="ciudad">Ciudad</label>
                        <input type="email" class="form-control" name="ciudad" value="{{ $file[0]->ciudad }}">
               </div>
                    <div class="form-group col-md-3">
                        <label for="estado">Estado</label>
                        <input type="text" class="form-control" name="estado" value="{{ $file[0]->Estado }}">
                    </div>

                </div>
                <div class="form-group">
                    <label for="email">Comentarios</label>
                    <input type="textarea" class="form-control" value="Aún esta pendiente de entregar documentación">
                </div>
            


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <a href="{{ ('/files/'.$file[0]->id .'/edit/') }}"
                        class="btn btn-primary">Guardar</a>
                    <a href="{{ ('/files/'.$file[0]->id .'/edit/') }}"
                        class="btn btn-primary"> Ver Solicitud</a>
                    
                    @if ($file[0]->status == 3 && $idUser != 1 )

                    <a href="{{ ('/files/'.$file[0]->id .'/edit/') }}"
                      class="btn btn-primary"> Aprobar</a>
                    @endif      
                    @if ($file[0]->status == 4  )

                    <a href="{{ ('/files/'.$file[0]->id .'/edit/') }}"
                      class="btn btn-primary"> Póliza</a>
                    @endif    
                   
                 
                    <!--  <a href="" download="Acme Documentation (ver. 2.0.1).txt">Download Text</a> -->

                </div>
           
        </form>
      
      </div>
      <div class="tab-pane fade" id="documentos" role="tabpanel" aria-labelledby="documentos-tab">

     

      
      <section style="margin-left: 20px">
      
        <h3>Documentos Archivados</h3>
        <div>
          <a class="example-image-link" href="{{ asset('images/15943186961584482356cfe.png') }}" data-lightbox="example-set" data-title="Comprobante Domicilio"><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
         
        </div>
      </section>


      </div>
      <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <div class="col-md-6"> <br>
          <p>Buró de crédito</p>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile1" name="domicilio" >
            <label class="custom-file-label" for="customFile">Subir archivo</label>
          </div>
        </div>
        <div class="col-md-6"> <br>
            <p>Buró legal</p>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="customFile1" name="ingresos" >
              <label class="custom-file-label" for="customFile">Subir archivo</label>
            </div>
          </div>
      </div>
    </div>

    <script>
       $(document).ready(function() {
        $("#lightgallery").lightGallery(); 
    });
    </script> 
      
      
      @stop