<!DOCTYPE html>
<html>
<head>
    <title>Abogados</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
     <!-- Tempusdominus Bbootstrap 4 -->
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

  <style>
    body{
   overflow-x: hidden;
}
    .form-control{
        border-radius: 15px;
    }
    #img{
        width: auto; height: 200px;
    }
    .btn{
        border-radius: 15px;
        background-color: #C1B493
    }
    .contenido{
        padding-top: 7%;
        padding-left: 7%;
        padding-right: 7%;
}
  h1{
      font-family: 'Vogue';
  }
 p {
        font-family: 'Montserrat';
        font-size: 15px;
        text-align: justify;


    }
    .contenido2{


    }
    #logo{
        width: auto; height: 60px;
        padding-left: 100px;
    }
    .ec-stars-wrapper {
	/* Espacio entre los inline-block (los hijos, los `a`)
	   http://ksesocss.blogspot.com/2012/03/display-inline-block-y-sus-empeno-en.html */
	font-size: 0;
	/* Podríamos quitarlo,
		pero de esta manera (siempre que no le demos padding),
		sólo aplicará la regla .ec-stars-wrapper:hover a cuando
		también se esté haciendo hover a alguna estrella */
	display: inline-block;
}
.ec-stars-wrapper a {
	text-decoration: none;
	display: inline-block;
	/* Volver a dar tamaño al texto */
	font-size: 32px;
	font-size: 2rem;

	color: #C1B493;
}

.ec-stars-wrapper:hover a {
	color: rgb(39, 130, 228);
}
/*
 * El selector de hijo, es necesario para aumentar la especifidad
 */
.ec-stars-wrapper > a:hover ~ a {
	color: #C1B493;

}
a{
     color: #C1B493;
   }
#forContact{
  font-family: 'Vogue';
}
</style>

</head>
<body>
    <header>

        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="https://polizaderentas.com">
              <img id="logo" src="{{ asset("img/wp/logo.png") }}">
            </a>
          </nav>
      </header>

    <main role="main">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="card-img" src="{{ asset("img/wp/header-abogados.jpg") }}" alt="Card image">
              </div>

            </div>
          </div>


          <div class="contenido">
        <div class="row">

            <div class="col-md-4">

                <div class="card" >
                <img class="card-img-top" src="{{ asset("img/usuarios/". $users[0]->img_user ) }}" alt="{{ $users[0]->name }}">
                  <div class="card-body">
                  <p class="card-text"><strong>Lic. {{ $users[0]->name }}</strong></p>

                  <small>Puesto: Abogado</small> <br>
                  <small>Experiencia: {{ $users[0]->anio_exp }} </small> <br>
                  <div class="ec-stars-wrapper">
                    <a href="#" data-value="1" title="Votar con 1 estrellas">&#9733;</a>
                    <a href="#" data-value="2" title="Votar con 2 estrellas">&#9733;</a>
                    <a href="#" data-value="3" title="Votar con 3 estrellas">&#9733;</a>
                    <a href="#" data-value="4" title="Votar con 4 estrellas">&#9733;</a>
                    <a href="#" data-value="5" title="Votar con 5 estrellas">&#9733;</a>
                  </div><br>
                  <small>Email: {{ $users[0]->email}}</small> <br>
                  <small><a href="tel:+5200{{ $users[0]->telefono_user }}">Télefono: {{ $users[0]->telefono_user }}</a></small> <br>

                  <hr>
                  <small>Contacto: </small>


                    <div class="d-flex justify-content-between align-items-center">

                    <small class="text-muted"><i class="fab fa-facebook-f fa-xs"></i></small><br>
                    <small class="text-muted"><i class="fab far fa-envelope fa-xs"></i></small>
                  </div>
                  </div>
                </div>

            </div>


            <div class="col-md-7">

                <h1>Experiencia</h1>
                <p>
                    {{ $users[0]->bio_user }}
                </p>

                @include('directorio.create')

            </div>


          </div>

        </div> <!--cierra contenido-->




        <!--formulario de contacto -->


      </main>


</body>

</html>
