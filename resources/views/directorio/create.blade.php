      <!--Formulario-->

     

        
        <div class="col-md-12">
            
           <h4 id="forContact">Contacta al Lic.  {{ $users[0]->name }}</h4>

           <p>Envíame mensaje y en breve me pondré en contacto contigo.</p>
    <form role="form" action="{{ url('sucursales') }}" method="post">

        <div class="card-body">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-4">

                    <input type="text" name="nombres" required class="form-control" placeholder="Nombre(s)">
                </div>
                <div class="form-group col-md-4">

                    <input type="text" name="apellidoP" required class="form-control" placeholder="Primer Apellido">
                </div>
                <div class="form-group col-md-4">

                    <input type="text" name="apellidoM" required class="form-control" placeholder=" Segundo Apellido">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">

                    <input type="text" name="celular" class="form-control" placeholder="Teléfono">
                </div>
                    <div class="form-group col-md-6">
                    <input type="email" name="email" required class="form-control" placeholder="Correo Electrónico">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-xl-12">

                    <textarea name="mensaje" required class="form-control" placeholder="Deja aquí tu comentario" rows="5" cols="40"></textarea>
                    <input type="hidden" name="email_suc" value="{{ $users[0]->email_suc }}" class="form-group">
                    <input type="hidden" name="idUser" value="{{ $users[0]->idUser }}" class="form-group">
                    <input type="hidden" name="idSucursal" value="{{ $users[0]->idSucursal }}" class="form-group">
                    <input type="hidden" name="emailUser" value="{{ $users[0]->email }}" class="form-group">
                    <input type="hidden" name="status_online" value="0" class="form-group">
                    <input type="hidden" name="formulario" value="abogado" class="form-group">
                </div>
            </div>



            <div class="form-row">


             <input type="hidden" name="status" value="1">


                <div class="form-group ">
                    <label for="submit"> <button type="submit"  class="btn"  style="color:#292824" >ENVIAR</i></button>
                   </label>
                </div>

            </div>



        </div>
        <!-- /.card-body -->

    </form>


    </div>
<!--Fin del resultado-->


<!--Fin del row-->

<!--FIN DEL FORMULARIO-->
