@extends('layouts.admin')

@section('content')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Listas Landig Page</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
            <li class="breadcrumb-item active">Usuarios</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 ">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						
					</div>
					<div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Rol</th>
                                <th>Sucursal</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                                @foreach ($users as $user )
                                    <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    
                                    <td>Role</td>
                                      <td>Toluca</td>
                                    <td>
                                
                                    <a href="{{ route('directorio.show', $user->id) }}" class="btn btn-primary">Ver</a>
                                  

                                 @include('users.delete', ['user'=>$user])
                               
                                    </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>


@endsection