      <!--Formulario-->

      <div class="row" id="formulario">
        <div class="col-sm-5">

                <div class="form-group">
                    <img class="example-image" id="familia" src="{{ asset('img/wp/pareja-banner.png') }}" alt=""/>
                  </div>

        </div>
        <div class="col-sm-6">
           
           <h4 id="forContact">CONSULTA CON LOS PROFESIONALES</h4>

           <p>Envíanos mensaje y en breve uno de nuestros aesores se pondrá en contacto. Gracias por tu confianza.</p>
    <form role="form" action="{{ url('sucursales') }}" method="post">

        <div class="card-body">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-4">

                    <input type="text" name="nombres" required class="form-control" placeholder="Nombre(s)">
                </div>
                <div class="form-group col-md-4">

                    <input type="text" name="apellidoP" required class="form-control" placeholder="Apellido Paterno">
                </div>
                <div class="form-group col-md-4">

                    <input type="text" name="apellidoM"  class="form-control" placeholder="Apellido Materno">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">

                    <input type="text" name="celular" class="form-control" placeholder="Teléfono">
                </div>
                    <div class="form-group col-md-6">
                    <input type="email" name="email" required class="form-control" placeholder="Correo Electrónico">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-xl-12">


                    <textarea name="mensaje" required class="form-control" placeholder="Deja aquí tu comentario" rows="5" cols="40"></textarea>
                    <input type="hidden" name="idUser" value="" class="form-group">
                    <input type="hidden" name="email_suc" value="{{ $sucursal[0]->email_suc }}" class="form-group">
                    <input type="hidden" name="idSucursal" value="{{ $sucursal[0]->id }}" class="form-group">
                    <input type="hidden" name="status" value="0" class="form-group">
                    <input type="hidden" name="formulario" value="sucursal" class="form-group">
                </div>
            </div>



            <div class="form-row">


             <input type="hidden" name="status" value="1">





                <div class="form-group ">
                   <label for="submit"> <button type="submit"  class="btn"  style="color:#ffffff" >ENVIAR</i></button>
                   </label>
                </div>

            </div>



        </div>
        <!-- /.card-body -->

    </form>


    </div>
<!--Fin del resultado-->
    <div class="col-sm-1"></div>
</div>
<!--Fin del row-->

<!--FIN DEL FORMULARIO-->
