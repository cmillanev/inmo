<!DOCTYPE html>
<html>
<head>
    <title>Sucursales</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
     <!-- Tempusdominus Bbootstrap 4 -->
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
   <!-- Font Awesome -->
   <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

  <style>

body{
   overflow-x: hidden;
}

.slider-wrap{
	position: relative;
	display: flex;
	justify-content: center;
    width: 100%;
}

.slider-container{
	position: absolute;
	top: 20%;
    left: 30%;

	z-index: 999;
	width: 400px;
    display: block;
}

.form-title{
	width: 100%;
	padding: 12px;
	background: rgba(255, 255, 255, 0.9);

  }


.form-title h1{
	font-family: "Open sans";
	font-size: 25px;
	font-weight: 200;
	text-align: center;
	color: #fff;
}

.slider-form{
	width: 100%;
	background: rgba(255, 255, 255, 0.9);
	padding: 25px;
  border-radius: 15px;
}

.slider-form input[type=text]{
	width: 100%;
	padding: 8px;
	margin: 8px 0;
}

.slider-form input[type=button]{
	width: 100px;
	padding: 5px;
	margin-top: 10px;
	border: none;
	background: #2D99CC;
	color: #fff;
	cursor:pointer;
}

/*! http://responsiveslides.com v1.55 by @viljamis */

.rslides {
  position: relative;
  list-style: none;
  overflow: hidden;
  width: auto;
  padding: 0;
  margin: 0;
  }

.rslides li {
  -webkit-backface-visibility: hidden;
  position: absolute;
  display: none;
  width: auto;
  left: 0;
  top: 0;
  }

.rslides li:first-child {
  position: relative;
  display: block;
  float: left;
  }

.rslides img {
  display: block;
  height: auto;
  float: left;
  width: auto;
  border: 0;

  }

  .form-control{
            border-radius: 15px;
        }

  .texto {
  text-align: center;
}

.card-title{
    color: #fff;
    padding-top: 30px;
    padding-bottom: 30px;
    font-family: 'Vogue';
}

.card-text{
    color: #fff;
    text-align: left;
}

.card-img{

  filter:drop-shadow(8px 8px 10px rgb(12, 12, 12));
  height: 270px;

}
.contenido{
 font-family: 'Vogue';
  height: 270px;
  text-align: center;
}
.mapa{

  height: 270px;
  filter:drop-shadow(8px 8px 10px rgb(12, 12, 12));
}
#logo{
        width: auto; height: 60px;
        padding-left: 100px;
    }

h5{
  font-family: 'Montserrat';
}



  </style>
</head>
<body>
  <header>

    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="https://polizaderentas.com">
          <img id="logo" src="{{ asset("img/wp/logo.png") }}">
        </a>
      </nav>
  </header>



        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100"  src="{{ asset("img/wp/cdmx.jpg") }}" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100"  src="{{ asset("img/wp/polizas.jpg") }}" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100"  src="{{ asset("img/wp/toluca.jpg") }}" alt="Third slide">
              </div>
            </div>
          </div>


		<div class="slider-container">

			<form action="" class="slider-form">
        <h5>Encuentra tu</h5>
        <h5>sucursal más cercana</h5>


        <select name="estado" id="estado" class="form-control btn-submit" >
          <option value="" selected>Estado</option>

          @foreach($sucursales as $sucursal)

              <option value="{{ $sucursal->estado }}">{{ $sucursal->estado }}</option>

          @endforeach
        </select>


			</form>
		</div>



  <!--Resultado-->


      <div class=" contenedor" id="f315-div"></div>

<!--Fin del resultado-->
</body>

<script type="text/javascript">


$(function() {
    $('.carousel').carousel({
  interval: 2000
    })
});




$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-submit").change(function(e){

        e.preventDefault();

        var name = $("select[name=estado]").val();
        var mun = "";



        $.ajax({
           type:'POST',
           url:"{{ route('sucursal.post') }}",
           data:{name:name, mun:mun},
           success: function (response) {
                $('#f315-div').html(response);
            },
        });

	});




  /*! http://responsiveslides.com v1.55 by @viljamis */
(function(c,K,C){c.fn.responsiveSlides=function(m){var a=c.extend({auto:!0,speed:500,timeout:4E3,pager:!1,nav:!1,random:!1,pause:!1,pauseControls:!0,prevText:"Previous",nextText:"Next",maxwidth:"",navContainer:"",manualControls:"",namespace:"rslides",before:c.noop,after:c.noop},m);return this.each(function(){C++;var f=c(this),u,t,v,n,q,r,p=0,e=f.children(),D=e.length,h=parseFloat(a.speed),E=parseFloat(a.timeout),w=parseFloat(a.maxwidth),g=a.namespace,d=g+C,F=g+"_nav "+d+"_nav",x=g+"_here",k=d+"_on",
y=d+"_s",l=c("<ul class='"+g+"_tabs "+d+"_tabs' />"),z={"float":"left",position:"relative",opacity:1,zIndex:2},A={"float":"none",position:"absolute",opacity:0,zIndex:1},G=function(){var b=(document.body||document.documentElement).style,a="transition";if("string"===typeof b[a])return!0;u=["Moz","Webkit","Khtml","O","ms"];var a=a.charAt(0).toUpperCase()+a.substr(1),c;for(c=0;c<u.length;c++)if("string"===typeof b[u[c]+a])return!0;return!1}(),B=function(b){a.before(b);G?(e.removeClass(k).css(A).eq(b).addClass(k).css(z),
p=b,setTimeout(function(){a.after(b)},h)):e.stop().fadeOut(h,function(){c(this).removeClass(k).css(A).css("opacity",1)}).eq(b).fadeIn(h,function(){c(this).addClass(k).css(z);a.after(b);p=b})};a.random&&(e.sort(function(){return Math.round(Math.random())-.5}),f.empty().append(e));e.each(function(a){this.id=y+a});f.addClass(g+" "+d);m&&m.maxwidth&&f.css("max-width",w);e.hide().css(A).eq(0).addClass(k).css(z).show();G&&e.show().css({"-webkit-transition":"opacity "+h+"ms ease-in-out","-moz-transition":"opacity "+
h+"ms ease-in-out","-o-transition":"opacity "+h+"ms ease-in-out",transition:"opacity "+h+"ms ease-in-out"});if(1<e.length){if(E<h+100)return;if(a.pager&&!a.manualControls){var H=[];e.each(function(a){a+=1;H+="<li><a href='#' class='"+y+a+"'>"+a+"</a></li>"});l.append(H);m.navContainer?c(a.navContainer).append(l):f.after(l)}a.manualControls&&(l=c(a.manualControls),l.addClass(g+"_tabs "+d+"_tabs"));(a.pager||a.manualControls)&&l.find("li").each(function(a){c(this).addClass(y+(a+1))});if(a.pager||a.manualControls)r=
l.find("a"),t=function(a){r.closest("li").removeClass(x).eq(a).addClass(x)};a.auto&&(v=function(){q=setInterval(function(){e.stop(!0,!0);var b=p+1<D?p+1:0;(a.pager||a.manualControls)&&t(b);B(b)},E)},v());n=function(){a.auto&&(clearInterval(q),v())};a.pause&&f.hover(function(){clearInterval(q)},function(){n()});if(a.pager||a.manualControls)r.bind("click",function(b){b.preventDefault();a.pauseControls||n();b=r.index(this);p===b||c("."+k).queue("fx").length||(t(b),B(b))}).eq(0).closest("li").addClass(x),
a.pauseControls&&r.hover(function(){clearInterval(q)},function(){n()});if(a.nav){g="<a href='#' class='"+F+" prev'>"+a.prevText+"</a><a href='#' class='"+F+" next'>"+a.nextText+"</a>";m.navContainer?c(a.navContainer).append(g):f.after(g);var d=c("."+d+"_nav"),I=d.filter(".prev");d.bind("click",function(b){b.preventDefault();b=c("."+k);if(!b.queue("fx").length){var d=e.index(b);b=d-1;d=d+1<D?p+1:0;B(c(this)[0]===I[0]?b:d);(a.pager||a.manualControls)&&t(c(this)[0]===I[0]?b:d);a.pauseControls||n()}});
a.pauseControls&&d.hover(function(){clearInterval(q)},function(){n()})}}if("undefined"===typeof document.body.style.maxWidth&&m.maxwidth){var J=function(){f.css("width","100%");f.width()>w&&f.css("width",w)};J();c(K).bind("resize",function(){J()})}})}})(jQuery,this,0);

  </script>


</html>
