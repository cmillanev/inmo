@foreach ($sucursales as $sucursal)

<div class="row">
  <div class="col-sm-8 contenido">
    <div class="row">
     <img class="card-img" src="{{ asset("img/sucursales/". $sucursal->img_suc ) }}" alt="" >
     <div class="card-img-overlay">
       <div class="row">
    <div class="col-sm-6">
     
      <h2 class="card-title ">{{ $sucursal->nombre_suc }}</h2>

      <a href="{{ route('sucursales.show', $sucursal->nombre_suc) }}" class="btn btn-outline-light  "><h3>CONOCE LA SUCURSAL</h3></a>
    </div>
    <div class="col-sm-6">
      <h5 class="card-text"><i class="far fa-building fa-2x"> </i> {{$sucursal->calle . ',  No. ' . $sucursal->numExt . ', ' .$sucursal->colonia . ', ' . $sucursal->municipio . '. ' . $sucursal->estado }}</h5>
               
                <h5 class="card-text "><i class="fab fa-whatsapp fa-2x"> </i> {{$sucursal->telefono_suc}}</h5>
                
                <h5 class="card-text"><i class="far fa-envelope fa-2x"> </i> {{$sucursal->email_suc}}</h5>
    </div>
  </div>
     </div>
    </div>
  </div>
  <div class="col-sm-4 mapa" id="mapid" style="width: auto;">
   
    
  </div>
</div>



<script>
     var sucursales = @JSON($sucursal);
     console.log(sucursales.lat);
     var mymap = L.map('mapid').setView([sucursales.lat, sucursales.lng], 17);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
  maxZoom: 18,
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1
}).addTo(mymap);


L.marker([sucursales.lat, sucursales.lng]).addTo(mymap);




var popup = L.popup();

function onMapClick(e) {
  popup
    .setLatLng(e.latlng)
    .setContent("You clicked the map at " + e.latlng.toString())
    .openOn(mymap);
}

mymap.on('click', onMapClick);
  </script>
@endforeach

