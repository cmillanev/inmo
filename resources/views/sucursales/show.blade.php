<!DOCTYPE html>
<html>
<head>
    <title>Sucursales</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
     <!-- Tempusdominus Bbootstrap 4 -->
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
   <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('fonts/Vogue.ttf') }}">
   <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
   <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;1,400&display=swap" rel="stylesheet">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

  <style>
      body{
   overflow-x: hidden;
}
    .form-control{
        border-radius: 15px;
    }
    #img{
        width: auto; height: 200px;
    }
    .btn{
        border-radius: 15px;
        background-color: black
    }
    .contenido{
        padding: 10%;

        background-image: url('../img/wp/medio-escudo.png');
        background-repeat: no-repeat;
        background-size: 300px 380px;

    }
    .card{



    }


    .text-muted{
        padding-top: 10px;

    }

    #titulo{
        font-family: 'Vogue';
        text-align; left;
    }

    #contenido{
        font-family: 'Montserrat';
        font-size: 15px;
        text-align: justify;


    }

    #familia{

    width: 420px; height: 420px;

    }
    #carouselExampleSlidesOnly{
        padding-bottom: 7%;
    }
    #logo{
        width: auto; height: 60px;
        padding-left: 100px;
    }
    #formulario{
        background-color: #F6F6F6;


    }
   a{
     color: #C1B493;
   }
   .card-img-top{

   }
   #forContact{
    font-family: 'Vogue';
    padding-top: 20px;
   }
   p{
    font-family: 'Montserrat';
   }
</style>

</head>
<body>
    <header>

        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="https://polizaderentas.com">
              <img id="logo" src="{{ asset("img/wp/logo.png") }}">
            </a>
          </nav>
      </header>

    <main role="main">

        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="card-img" src="{{ asset("img/sucursales/". $sucursal[0]->img_suc ) }}" alt="Card image">
              </div>

            </div>
          </div>


        <div class="contenido">
        <div class="row">

            <div class="col-md-2"></div>
            <div class="col-md-7">

                <h1 id="titulo">{{ $sucursal[0]->nombre_suc }}</h1>
                <p id="contenido">
                   {{ $sucursal[0]->descripcion_suc }}
                </p>



            </div> <!--Fin de contenido-->
            <div class="col-md-3">
                <p><i class="far fa-building fa-3x" style="color:#C1B493"> </i> {{ $sucursal[0]->calle . ',  No. ' . $sucursal[0]->numExt . ', ' .$sucursal[0]->colonia . ', ' . $sucursal[0]->municipio . '. ' . $sucursal[0]->estado }}</p>
                <p><i class="fab fa-whatsapp fa-3x" style="color:#C1B493"> </i> {{ $sucursal[0]->telefono_suc }}</p>
                <p><i class="far fa-envelope fa-3x" style="color:#C1B493"> </i> {{ $sucursal[0]->email_suc }}</p>
            </div>
          </div>
        </div>

        <div class="contenido2">

        <div class="row">
            <div class="col-md-4">
                <div id="mapid" style="width: 100%; height: 350px;"></div>
            </div>

            @foreach ($users as $usuario)
            <div class="col-md-2" >

                <a href="{{ route('directorio.show', $usuario->name) }}">
                <div class="card" >
                <img class="card-img-top" src="{{ asset("img/usuarios/". $usuario->img_user ) }}" alt="{{ $usuario->name }}">
                  <div class="card-body">
                  <p class="card-text"><strong>Lic. {{ $usuario->name }}</strong></p>
                    <div class="d-flex justify-content-between align-items-center">
                      </a>
                        <small class="text-muted"><i class="fas fa-phone fa-xs" > </i> {{ $usuario->telefono_user }}</small>
                        <small class="text-muted"><i class="fab fa-facebook-f fa-xs"></i></small>
                        <small class="text-muted"><i class="fab far fa-envelope fa-xs"></i></small>
                     </div>

                  </div>
                </div>

            </div>
                    @endforeach
         </div>
        </div>



        @include('sucursales.create')

      </main>

      <script>
         var sucursales = @JSON($sucursal);

        var mymap = L.map('mapid').setView([sucursales[0].lat, sucursales[0].lng], 17);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      draggable: false
    }).addTo(mymap);

    L.marker([sucursales[0].lat, sucursales[0].lng]).addTo(mymap);




    var popup = L.popup();

    function onMapClick(e) {
      popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(mymap);
    }

    mymap.on('click', onMapClick);
      </script>


</body>

</html>
