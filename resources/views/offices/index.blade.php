@extends('layouts.admin')


@section('content')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Sucursales</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
            <li class="breadcrumb-item active">Sucursales</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12 ">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">

                    <div class="box-tools pull-right " >


                      @switch($idRol)
                          @case(4)
                          <a href=" {{ url('offices/create') }} " class="btn btn-success">Nueva Sucursal</a>
                              @break
                          @case(5)
                          <a href=" {{ url('offices/create') }} " class="btn btn-success">Nueva Sucursal</a>
                              @break
                          @default

                      @endswitch
                  </div>

                </div>
                <div class="box-body">

                  <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Código</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($offices as $office )
                                <tr>
                                <td>{{ $office->nombre_suc }}</td>
                                <td>{{ $office->email_suc }}</td>
                                <td>{{ $office->codigo }}</td>
                                <td>


                                  @if ($idRol == 2 || $idRol == 1)
                                  <a href="{{ route('sucursales.show', $office->nombre_suc) }}" class="btn btn-primary">Ver</a>
                                  @endif
                                  @if ($idRol == 3)
                                  <a href="{{ route('sucursales.show', $office->nombre_suc) }}" class="btn btn-primary">Ver</a>
                                  <a href="{{ route('offices.edit', $office->id) }}" class="btn btn-primary">Editar</a>
                                  @endif
                                  @if ($idRol == 4 ||  $idRol == 5)
                                  <a href="{{ route('sucursales.show', $office->nombre_suc) }}" class="btn btn-primary">Ver</a>
                                  <a href="{{ route('offices.edit', $office->id) }}" class="btn btn-primary">Editar</a>
                                  @include('offices.delete', ['office'=>$office])
                                  @endif



                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
    </div>
</div>

@endsection
