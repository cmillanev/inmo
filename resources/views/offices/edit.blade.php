@extends('layouts.admin')



@section('content')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Editar Sucursal</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ url('sucursales') }}">Sucursales</a></li>
            <li class="breadcrumb-item active"> Nueva Sucursal</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>


  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title">Ingresa los datos</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ route('offices.update',  $office->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
      <div class="card-body">

       <div class="row">
        <div class="form-group col-md-3">
            <label for="nombre">Nombre de sucursal</label>
            <input type="text" name="nombre_suc" value="{{ $office->nombre_suc }}" class="form-control">
        </div>
        <div class="form-group col-md-2">
            <label for="email">Email Sucursal</label>
            <input type="email" name="email_suc" value="{{ $office->email_suc }}" class="form-control">
        </div>
        <div class="form-group col-md-2">
            <label for="password">Código Sucursal</label>
            <input type="text" name="codigo" value="{{ $office->codigo }}" class="form-control">
        </div>
        <div class="form-group col-md-3">
            <label for="calle">Calle</label>
            <input type="text" name="calle" value="{{ $office->calle }}" class="form-control">
        </div>
        <div class="form-group col-md-2">
            <label for="password">Numero Ext</label>
            <input type="text" name="numExt" value="{{ $office->numExt }}" class="form-control">
        </div>
      </div>
      <div class="row">

        <div class="form-group col-md-1">
            <label for="password">Num Interior</label>
            <input type="text" name="numInt" value="{{ $office->numInt }}" class="form-control">
        </div>

        <div class="form-group col-md-2">
            <label for="password">Colonia</label>
            <input type="text" name="colonia" value="{{ $office->colonia }}" class="form-control">
        </div>
        <div class="form-group col-md-2">
            <label for="password">Municipio</label>
            <input type="text" name="municipio" value="{{ $office->municipio }}" class="form-control">
        </div>
        <div class="form-group col-md-2">
            <label for="password">Estado</label>
            <input type="text" name="estado" value="{{ $office->estado }}" class="form-control">
        </div>
        <div class="form-group col-md-1">
            <label for="password">CP</label>
            <input type="text" name="cp" value="{{ $office->cp }}" class="form-control">
        </div>
        <div class="form-group col-md-2">
            <label for="password">Teléfono</label>
            <input type="text" name="telefono_suc" value="{{ $office->telefono_suc }}" class="form-control">
        </div>
        <div class="form-group col-md-1">
            <label for="password">Lat</label>
            <input type="text" name="lat" value="{{ $office->lat }}" class="form-control">
        </div>
        <div class="form-group col-md-1">
            <label for="password">Lng</label>
            <input type="text" name="lng" value="{{ $office->lng }}"  class="form-control">
        </div>
      </div>
      <div class="row">
        <div class="col-md-6"> <br>
          <p>Ciudad</p>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile" name="img_ciudad" >
            <label class="custom-file-label" for="customFile" >Seleccionar archivo</label>
          </div>
        </div>
        <div class="col-md-6"> <br>
          <p>Sucursal</p>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile1" name="img_suc" >
            <label class="custom-file-label" for="customFile">Seleccionar archivo</label>
          </div>
        </div>

          </div>
      </div>
      <div class="row">
          <div class="form-group col-md-8 ">
            <input class="form-control" type="text" name="descripcion_suc" value="{{ $office->descripcion_suc }}">
          </div>
      </div>

      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
      <!-- /.card-body -->

    </form>


  </div>
  <!-- /.card -->


  <div id="map"></div>
@endsection


<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD74Y8NCL6W0xEGVZvgGjUIZ4UYHz8ZXQM&callback=initMap&libraries=&v=weekly"
      defer
    ></script>
<style type="text/css">
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 30%;
        width: 30%;

      }



      #floating-panel {

        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: "Roboto", "sans-serif";
        line-height: 30px;
        padding-left: 10px;
      }
    </style>

    <script>
      function initMap() {
  var map = new google.maps.Map(document.getElementById("map"), {
    zoom: 12,
    center: { lat: 19.34, lng: -99.644 }
  });
  var geocoder = new google.maps.Geocoder();

  document.getElementById("submit1").addEventListener("click", function() {
    geocodeAddress(geocoder, map);
  });
}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById("address").value;
  geocoder.geocode({ address: address }, function(results, status) {
    if (status === "OK") {
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location

      });
      console.log(results);
      console.log(results[0].geometry.location.lat);
      console.log(results[0].geometry.location.lng);
    } else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });
}
    </script>
