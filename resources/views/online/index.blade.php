@extends('layouts.admin')


@section('content')


<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Solicitudes Online</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
            <li class="breadcrumb-item active">Solicitudes</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
    

<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12 ">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                   
              
                    <div class="box-tools pull-right " >
                    <a href="{{ url('online/create') }}" class="btn btn-success">Enviar Solicitud</a>
                   
                    </div>
                
                
                </div>
                <div class="box-body">
                                  <!-- Filtro -->
            <div class="form-group">
              <label>Date range button:</label>
              <div class="input-group-btn">
                <button type="button" class="btn btn-default" id="daterange-btn" style='width:230px'>
                    <i class="fa fa-calendar"></i>&nbsp; <span>defaut date</span>
                  <i class="fa fa-caret-down"></i>
                </button>
                <button id='btnDec' type="button" class="btn btn-danger btn-flat" title='Decrement month'><i class="far fa-calendar-minus" aria-hidden="true"></i></button>
                <button id='btnInc' type="button" class="btn btn-info btn-flat" title='Increment month'><i class="far fa-calendar-plus" aria-hidden="true"></i></button>
              </div>
            </div>
            <!-- /.filtro -->
                  <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <th>Nombre</th>
                            <th>Celular</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($datos as $file)

                                <tr>                            
                                  <td>{{ $file->nombres }} {{ $file->apellidoP }} {{ $file->apellidoM }}</td>
                                  <td>{{ $file->celular }}</td>
                                  <td>{{ $file->email }}</td>
                                  <td>{{ $file->fecha }}</td>
                                   <td>
                               
                                      <a href="" class="btn btn-primary">Ver</a>
                           
                                      <a href="" class="btn btn-danger">Eliminar</a>
    
                                </td>
                          
                                </tr>
                             @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
    </div>
</div>



@endsection