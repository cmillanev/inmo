@extends('layouts.admin')



@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Nuevo Expediente</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('online') }}">Online</a></li>
                    <li class="breadcrumb-item active"> Nuevo Expediente</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title">Formato de captura</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ url('online') }}" method="post">

        <div class="card-body">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="nombre">Nombre (s)</label>
                    <input type="text" name="nombres" required class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label for="apellidoP">Apellido Paterno</label>
                    <input type="text" name="apellidoP" required class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label for="apellidM">Apellido Materno</label>
                    <input type="text" name="apellidoM" required class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="email">Email</label>
                    <input type="email" name="email" required class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label for="password">Celular</label>
                    <input type="text" name="celular" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label for="rol">Seleccione una sucursal</label>
                    <select name="idSuc" id="" class="form-control">

                        @foreach($offices as $office)

                            <option value="{{ $office->id }}">{{ $office->nombre_suc }}</option>

                        @endforeach

                    </select>

                </div>
                <div class="form-group col-md-3">
                    <label for="rol">Seleccione un asesor</label>
                    <select name="idUser" id="" class="form-control">

                        @foreach($users as $user)

                            <option value="{{ $user->id }}">{{ $user->name }}</option>

                        @endforeach

                    </select>

                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="renta">Renta</label>
                    <input type="text" class="form-control" name="renta" value="">
                </div>
                <div class="form-group col-md-2">
                    <label for="montoPoliza">Monto Póliza</label>
                    <input type="text" class="form-control" name="montoPoliza" value="">
                </div>
                <div class="form-group col-md-4">
                    <label for="calle">Calle</label>
                    <input type="text" class="form-control" name="calle" value="">
                </div>
                <div class="form-group col-md-2">
                    <label for="calle">Número Ext</label>
                    <input type="text" class="form-control" name="numExt" value="">
                </div>
                <div class="form-group col-md-2">
                    <label for="calle">Número Int</label>
                    <input type="text" class="form-control" name="numInt" value="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="referenciasUnicacion">Referencias Ubicación</label>
                    <input type="text" class="form-control" name="referenciasUbicacion" value="">
                </div>
                <div class="form-group col-md-3">
                    <label for="ciudad">Ciudad</label>
                    <input type="text" class="form-control" name="ciudad" value="">
                </div>
                <div class="form-group col-md-3">
                    <label for="celular">Estado</label>
                    <input type="text" class="form-control" name="estado" value="">
                </div>
                <input type="hidden" name="lat" value="18.3423">
                <input type="hidden" name="lng" value="-99.2342">
            </div>
            <div class="form-group">

                <input type="hidden" name="status" value="1">
            </div>


            <div class="box-tools pull-right ">
                <input type="submit" value="Enviar" class="btn btn-success">
            </div>
        </div>
        <!-- /.card-body -->

    </form>
</div>
<!-- /.card -->
@endsection
