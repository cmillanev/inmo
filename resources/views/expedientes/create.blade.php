
<!DOCTYPE html>
<html>
<head>
    <title>Solicitud en línea</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
     <!-- Tempusdominus Bbootstrap 4 -->
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
   <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <style>
        .form-control{
            border-radius: 15px;
        }
        #img{
            width: 250px; height: 200px;
        }
        .btn{
            border-radius: 15px;
        }
        #logo{
        width: auto; height: 60px;
        padding-left: 100px;
    }
    </style>
</head>
<body>
    <header>

        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="https://polizaderentas.com">
              <img id="logo" src="{{ asset("img/wp/logo.png") }}">
            </a>
          </nav>
      </header>

    <div class="container">

        <div class="row">
        <div class="col-sm-3">
            <br>

            <div class="row">


                <div class="form-group">
                    <img class="example-image" id="img" src="{{ asset('img/wp/gs1.jpg') }}" alt=""/>

                  </div>

            </div>
            <div class="row">
                <div class="form-group">
                    <img class="example-image" id="img" src="{{ asset('img/wp/gs2.jpg') }}" alt=""/>

                  </div>
            </div>

        </div>
        <div class="col-sm-9">
            <br>
           <h4>Genera tu solicitud en línea</h4>

           <p>Por favor ingresa tus datos. Nos comunicaremos contigo a la brevedad</p>
    <form role="form" action="{{ url('expedientes') }}" method="post">

        <div class="card-body">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-5">

                    <input type="text" name="nombres" required class="form-control" placeholder="Nombre(s)">
                </div>
                <div class="form-group col-md-3">

                    <input type="text" name="apellidoP" required class="form-control" placeholder="Apellido Paterno">
                </div>
                <div class="form-group col-md-3">

                    <input type="text" name="apellidoM" required class="form-control" placeholder="Apellido Materno">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">

                    <input type="text" name="celular" class="form-control" placeholder="Teléfono">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-7">

                    <input type="email" name="email" required class="form-control" placeholder="Correo Electrónico">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">

                    <select name="idSucursal" id="" class="form-control">
                        <option value="">Sucursal</option>
                        @foreach($offices as $office)


                            <option value="{{ $office->id }}">{{ $office->nombre_suc }}</option>

                        @endforeach

                    </select>

                </div>

            </div>
            <div class="form-row">
                <div class="form-group col-md-9">

                    <select name="idAsesor" id="" class="form-control">
                        <option value="">Abogado</option>
                        @foreach($users as $user)

                        @if ( $user->id == 3 || $user->id == 4 )

                        @else
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endif


                        @endforeach

                    </select>

                </div>
            </div>
            <br>
            <br>
            <div class="form-row">


             <input type="hidden" name="status" value="1">
             <div class="form-group col-md-6">
                <label for="vehicle2"> Acepto términos y condiciones</label>
                <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat">
             </div>

                <div class="form-group ">
                   <label for="submit">ENVIAR <button type="submit"  class="btn "><i class="fa fa-angle-right "></i></button>
                   </label>
                </div>

            </div>



        </div>
        <!-- /.card-body -->

    </form>


	</div>
<!--Fin del resultado-->

</div>
<!--Fin del row-->


</body>

</html>






