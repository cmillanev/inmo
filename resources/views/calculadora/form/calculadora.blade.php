

      <table class="table " >
        <thead>
          <tr>
            <th class="encabezado text-white" >DEDUCCIONES COMPROBABLES Ó DEDUCCIÓN CIEGA</th>
            <th class="encabezado text-white"></th>
          </tr>
        </thead>
        <tr>
          <td> <div class="col-sm">
            DEDUCCIÓN CIEGA 35%
        </div></td>
          <td><div class="col-sm"> ${{ round($deduccionCiega, 2) }}</div></td>
        </tr>
        <tr>
          <td class="renglon text-white">
            <div class="col-sm">BASE</div>
          </td>
          <td class="renglon text-white">
            <div class="col-sm">$ {{ round($base, 2) }}</div>
          </td>
        </tr>
        <tr>
          <td>
            <div class="col-sm">(-)LIMITE INFERIOR</div>
          </td>
          <td>
            <div class="col-sm">$ {{ round($limiteInferior, 2)}}</div>
          </td>
        </tr>
        <tr>
          <td class="renglon text-white">
            <div class="col-sm">(=)EXCEDENTE LIMITE INFERIOR</div>
          </td>
          <td class="renglon text-white">
            <div class="col-sm">$ {{ round($excedente, 2) }}</div>
          </td>
        </tr>
        <tr>
          <td>
            <div class="col-sm">(*) %IMPUESTO</div>
          </td>
          <td>
            <div class="col-sm">{{ $impuesto }}%</div>
          </td>
        </tr>
        <tr>
          <td class="renglon text-white">
            <div class="col-sm">
              (=) IMPUESTO MARGINAL
            </div>
          </td>
          <td class="renglon text-white">
            <div class="col-sm">$ {{ round($impuestoMarginal, 2) }}</div>
          </td>
        </tr>
        <tr>
          <td>
            <div class="col-sm">
              (+) CUAOTA FIJA  
              </div>
          </td>
          <td>
            <div class="col-sm">$ {{ round($cuotaFija, 2) }}</div>
          </td>
        </tr>
        <tr>
          <td class="renglon text-white">
            <div class="col-sm">
              (=) IMPUESTO
            </div>
          </td>
          <td class="renglon text-white">
            <div class="col-sm">$ {{ round($impuesto2, 2) }}</div>
          </td>
        </tr>
        <tr>
          <td>
            <div class="col-sm">
              (-) RETENCIONES DE ISR POR PERSONAL MORAL
            </div>
          </td>
          <td>
            <div class="col-sm"></div>

          </td>
        </tr>
        <tr>
          <td class="renglon text-white">
            <div class="col-sm">
              ISR A CARGO MENSUAL
            </div>
          </td>
          <td class="renglon text-white">
            <div class="col-sm">$ {{ round($isrCargoMen, 2) }}</div>
          </td>
        </tr>
      </table>
       
        
        
  

   
