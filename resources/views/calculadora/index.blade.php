<!DOCTYPE html>
<html>
<head>
    <title>Calculadora ISR</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- Tempusdominus Bbootstrap 4 -->
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<style>
		body{
  background:#E1E1E1;
  min-width: 200px; 
}

#Principal{
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: left;
  align-items: center;
  padding-top:5%;
}

#Calculadora {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.5);
  -moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.5); 
  -webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.5); 
}

#Pantalla{
  background: white;
  background:linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);
  font-size: 20px;
  height: 100px;
  width:100%;
  border:0;
  margin:0;
  text-align:right;
  padding:10px;
  color: white;
  font-family: 'Roboto Condensed', sans-serif;
}
#Teclado{
  background: #45484A;  
}

.button{  
  background: #45484A;
  font-family: 'Roboto Condensed', sans-serif;
  font-size: 20px;
  margin: 10px;
  padding: 20px;
  border: 0%;
  border-radius: 50%;
  color: #FCFCFD;
  width: 60px;
  height: 60px;
  min-width: 10px;
  min-height: 10px;
  transition: all .2s ease-in-out;
  -webkit-transition: all .2s ease-in-out;
  -moz-transition: all .2s ease-in-out;
}
.button:hover{  
  transform: scale(1.1);  
  -webkit-transform: scale(1.1);
  background: #5A656F;    
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.2);
}
.button:active{ 
  transform: scale(1);  
}

.renglon{
	background: #C1B493;
}
.encabezado{
  background: #45484A;
}
	</style>
</head>
<body>
  
    <div class="container">
    
<div class="row">
<div class="col-sm-4">
	<form >
<div id = "Principal">  
  <div id= "Calculadora">         
     <input id="Pantalla" type="textbox" class="textbox" name="name">
    <div id= "Teclado" class="buttons">
      
      <button class="button" value="7" type="button">7</button>
      <button class="button" value="8" type="button">8</button>
      <button class="button" value="9" type="button">9</button><br>
      <button class="button" value="4" type="button">4</button>
      <button class="button" value="5" type="button">5</button>
      <button class="button" value="6" type="button">6</button><br>  
      <button class="button" value="1" type="button">1</button>
      <button class="button" value="2" type="button">2</button>
      <button class="button" value="3" type="button">3</button><br> 
      <button class="button" value="AC" type="button">AC</button>
      <button class="button" value="0" type="button">0</button>
      <button class="button btn-submit" type="submit" value="=">OK</button>
     
    </div>
  </div>
</div>


</div>
<!--Fin de Col-->

</form>
	<!--Resultado-->
	<div class="col-sm-8">
		<div id="f315-div"></div>		
	</div>	
<!--Fin del resultado-->

</div>
<!--Fin del row-->
			
  
        
    </div>
  
</body>
<script type="text/javascript">
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   
    $(".btn-submit").click(function(e){
  
        e.preventDefault();
   
        var name = $("input[name=name]").val();
       
      
   
        $.ajax({
           type:'POST',
           url:"{{ route('calculo.post') }}",
           data:{name:name},
           success: function (response) {
                $('#f315-div').html(response);
            },
        });
  
	});



var ans = "";
var clear = false;
var calc = "";

  $("button").click(function() {
    var text = $(this).attr("value");
	console.log(text);
    if(parseInt(text, 10) == text || text === "." || text === "/" || text === "*" || text === "-" || text === "+" || text === "%") {
      if(clear === false) {
        calc += text;
        $(".textbox").val(calc);
      } else {
        calc = text;
        $(".textbox").val(calc);
        clear = false;
      }
    } else if(text === "AC") {
      calc = "";
      $(".textbox").val("");
   
    } 
     
  });

</script>
   
</html>