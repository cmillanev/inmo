<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conlines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idSucursal');
            $table->string('idUser')->nullable();
            $table->string('idOnline');
            $table->string('status');
            $table->string('formulario');
            $table->dateTime('fecha_mod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conlines');
    }
}
