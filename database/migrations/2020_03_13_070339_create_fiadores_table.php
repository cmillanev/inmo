<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fiadores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idPer');
            $table->string('apellidoPf')->nullable();
            $table->string('apellidoMf')->nullable();
            $table->string('nombref')->nullable();
            $table->string('telefonof')->nullable();
            $table->string('celularf')->nullable();
            $table->string('relacionf')->nullable();
            $table->string('tiempof')->nullable();
            $table->string('nacionalidadf')->nullable();
            $table->string('especifiquef')->nullable();
            $table->string('sexof')->nullable();
            $table->string('edoCivilf')->nullable();
            $table->string('emailf')->nullable();
            $table->string('identif')->nullable();
            $table->string('inef')->nullable();
            $table->string('fechaNacf')->nullable();
            $table->string('rfcf')->nullable();
            $table->string('curpf')->nullable();
            $table->string('callef')->nullable();
            $table->string('numExtf')->nullable();
            $table->string('numIntf')->nullable();
            $table->string('cpf')->nullable();
            $table->string('coloniaf')->nullable();
            $table->string('munif')->nullable();
            $table->string('estadof')->nullable();
            $table->string('profesionf')->nullable();
            $table->string('tipoEmpleof')->nullable();
            $table->string('telefonoEf')->nullable();
            $table->string('extEf')->nullable();
            $table->string('empresaf')->nullable();
            $table->string('calleEf')->nullable();
            $table->string('nunExtEf')->nullable();
            $table->string('numIntEf')->nullable();
            $table->string('cpEf')->nullable();
            $table->string('coloniaEf')->nullable();
            $table->string('muniEf')->nullable();
            $table->string('estadoEf')->nullable();
            $table->string('ingresof')->nullable();
            $table->date('fechaIngf')->nullable();
            $table->string('calleDf')->nullable();
            $table->string('numExtDf')->nullable();
            $table->string('numIntDf')->nullable();
            $table->string('cpDf')->nullable();
            $table->string('coloniaDf')->nullable();
            $table->string('muniDf')->nullable();
            $table->string('estadoDf')->nullable();
            $table->string('escrituraf')->nullable();
            $table->string('foliof')->nullable();
            $table->string('notariaf')->nullable();
            $table->date('fechaf')->nullable();
            $table->string('boletaf')->nullable();
            $table->boolean('autorizo')->nullable();
            $table->boolean('declaro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fiadores');
    }
}
