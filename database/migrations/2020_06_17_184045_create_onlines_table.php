<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('onlines', function (Blueprint $table) {
            $table->id();
            $table->string('nombres')->nullable();
            $table->string('apellidoP')->nullable();
            $table->string('apellidoM')->nullable();
            $table->string('email')->nullable();
            $table->string('celular')->nullable();
            $table->string('comentarios')->nullable();
            $table->dateTime('fecha_ini')->nullable();
            $table->dateTime('fecha_mod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('onlines');
    }
}
