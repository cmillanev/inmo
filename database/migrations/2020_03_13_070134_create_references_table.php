<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('references', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idPer');
            $table->string('apellidoPr1')->nullable();
            $table->string('apellidoMr1')->nullable();
            $table->string('nombrer1')->nullable();
            $table->string('relacionr1')->nullable();
            $table->string('telefonor1')->nullable();
            $table->string('apellidoPr2')->nullable();
            $table->string('apellidoMr2')->nullable();
            $table->string('nombrer2')->nullable();
            $table->string('relacionr2')->nullable();
            $table->string('telefonor2')->nullable();
            $table->string('apellidoPf1')->nullable();
            $table->string('apellidoMf1')->nullable();
            $table->string('nombref1')->nullable();
            $table->string('relacionf1')->nullable();
            $table->string('telefonof1')->nullable();
            $table->string('apellidoPf2')->nullable();
            $table->string('apellidoMf2')->nullable();
            $table->string('nombref2')->nullable();
            $table->string('relacionf2')->nullable();
            $table->string('telefonof2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('references');
    }
}
