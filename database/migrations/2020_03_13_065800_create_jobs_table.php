<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idPer')->unsigned();
            $table->string('profesion')->nullable();
            $table->string('tipoEmp')->nullable();
            $table->string('tel')->nullable();
            $table->string('ext')->nullable();
            $table->string('empresa')->nullable();
            $table->string('calle')->nullable();
            $table->string('numExt')->nullable();
            $table->string('numInt')->nullable();
            $table->string('cp')->nullable();
            $table->string('colonia')->nullable();
            $table->string('mun')->nullable();
            $table->string('estado')->nullable();
            $table->date('fechaIng')->nullable();
            $table->string('apellidoPj')->nullable();
            $table->string('apellidoMj')->nullable();
            $table->string('nombrej')->nullable();
            $table->string('telefonoj')->nullable();
            $table->string('extj')->nullable();
            $table->string('ingresos')->nullable();
            $table->string('ingresoFam')->nullable();
            $table->string('numPerIngre')->nullable();
            $table->string('numPerDep')->nullable();
            $table->string('apellidoPa')->nullable();
            $table->string('apellidoMa')->nullable();
            $table->string('nombrea')->nullable();
            $table->string('parentesco')->nullable();
            $table->string('telefonoA')->nullable();
            $table->string('empresaA')->nullable();
            $table->string('ingresoA')->nullable();
            
        });

     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
