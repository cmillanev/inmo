<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_suc');
            $table->string('codigo');
            $table->string('calle');
            $table->string('numInt')->nullable();
            $table->string('numExt');
            $table->string('colonia');
            $table->string('municipio');
            $table->string('estado');
            $table->string('cp');
            $table->string('email_suc');
            $table->string('telefono_suc');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('img_ciudad')->nullable();
            $table->string('img_suc')->nullable();
            $table->longText('descripcion_suc');
            $table->dateTime('fecha_ini')->nullable();
            $table->dateTime('fecha_mod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}
