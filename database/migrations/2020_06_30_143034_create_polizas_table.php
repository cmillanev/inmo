<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePolizasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polizas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idPer');
            $table->text('renta')->nullable();
            $table->text('montoPoliza')->nullable();
            $table->text('calle')->nullable();
            $table->text('numExt')->nullable();
            $table->text('numInt')->nullable();
            $table->text('referenciasUbicacion')->nullable();
            $table->text('ciudad')->nullable();
            $table->text('Estado')->nullable();
            $table->text('lat')->nullable();
            $table->text('lng')->nullable();
            $table->text('clave')->nullable();
            $table->text('qr')->nullable();
            $table->dateTime('fecha_ini')->nullable();
            $table->dateTime('fecha_mod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polizas');
    }
}
