<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculations', function (Blueprint $table) {
            $table->id();
            $table->double('limInferior')->nullable();
            $table->double('limSuperior')->nullable();
            $table->double('cuotaFija')->nullable();
            $table->double('porciento')->nullable();
            $table->dateTime('fecha_ini')->nullable();
            $table->dateTime('fecha_mod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculations');
    }
}
