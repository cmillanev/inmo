<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('apellidoP')->nullable();
            $table->string('apellidoM')->nullable();
            $table->string('nombres')->nullable();
            $table->boolean('nacionalidad')->nullable();
            $table->string('especifiqueN')->nullable();
            $table->string('sexo')->nullable();
            $table->string('edoCivil')->nullable();
            $table->string('email')->nullable();
            $table->string('iden')->nullable();
            $table->string('ine')->nullable();
            $table->date('fechaNac')->nullable();
            $table->string('rfc')->nullable();
            $table->string('curp')->nullable();
            $table->string('telefonoP')->nullable();
            $table->string('celular')->nullable();
            $table->string('apellidoPc')->nullable();
            $table->string('apellidoMc')->nullable();
            $table->string('nombrec')->nullable();
            $table->string('telefonoc')->nullable();
            $table->string('adultos')->nullable();
            $table->string('menores')->nullable();
            $table->string('cuantos')->nullable();
            $table->string('situacion')->nullable();
            $table->text('motivo')->nullable();
            $table->string('mascotas')->nullable();
            $table->string('especifique')->nullable();
            $table->string('calle')->nullable();
            $table->string('numInt')->nullable();
            $table->string('numExt')->nullable();
            $table->string('cp')->nullable();
            $table->string('colonia')->nullable();
            $table->string('mun')->nullable();
            $table->string('estado')->nullable();
            $table->string('apellidoPa')->nullable();
            $table->string('apellidoMa')->nullable();
            $table->string('nombrea')->nullable();
            $table->string('telefonoa')->nullable();
            $table->string('renta')->nullable();
            $table->string('anioRenta')->nullable();
            $table->string('nombreEmp')->nullable();
            $table->string('apellidoPe')->nullable();
            $table->string('apellidoMe')->nullable();
            $table->string('nombreRazon')->nullable();
            $table->string('escritura')->nullable();
            $table->string('notario')->nullable();
            $table->date('fechaConst')->nullable();
            $table->string('giro')->nullable();
            $table->string('telefonoE')->nullable();
            $table->string('extE')->nullable();
            $table->string('emailE')->nullable();
            $table->string('nombreF')->nullable();
            $table->string('correoF')->nullable();
            $table->boolean('dirFisc')->nullable();
            $table->string('rfcfis')->nullable();
            $table->string('calleF')->nullable();
            $table->string('numIntF')->nullable();
            $table->string('cpF')->nullable();
            $table->string('numExtF')->nullable();
            $table->string('coloniaF')->nullable();
            $table->string('munF')->nullable();
            $table->string('estadoF')->nullable();
            $table->dateTime('fecha_ini')->nullable();
            $table->dateTime('fecha_mod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personals');
    }
}
