<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idSucursal');
            $table->string('idUser');
            $table->string('idRol');
            $table->string('img_user')->nullable();
            $table->string('telefono_user')->nullable();
            $table->string('facebook_user')->nullable();
            $table->longText('bio_user')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('anio_exp')->nullable();
            $table->string('linkedIn')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controls');
    }
}
