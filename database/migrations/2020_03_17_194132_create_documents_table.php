<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idper');
            $table->string('identificacion')->nullable();
            $table->string('ingresos')->nullable();
            $table->string('idepropiedad')->nullable();
            $table->string('propiedad')->nullable();
            $table->string('domicilio')->nullable();
            $table->string('dIne')->nullable();
            $table->string('dDomicilio')->nullable();
            $table->dateTime('fecha_ini')->nullable();
            $table->dateTime('fecha_mod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
