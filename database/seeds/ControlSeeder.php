<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ControlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ABOGADO JR TOLUCA
        DB::table('controls')->insert([
            'idSucursal' => '1',
            'idUser' => '1',
            'idRol' => '3',
            'img_user' => 'sandra.jpg',
            'telefono_user' => '7224024459',
            'facebook_user' => 'polizaderentas',
            'bio_user' => 'Lorem, lorem'
        ]);
        //ABOGADO
        DB::table('controls')->insert([
            'idSucursal' => '2',
            'idUser' => '2',
            'idRol' => '3',
            'img_user' => 'fernanda.jpg',
            'telefono_user' => '5621757040',
            'facebook_user' => 'polizaderentas',
            'bio_user' => 'Lorem, lorem'
        ]);

        DB::table('controls')->insert([
            'idSucursal' => '0',
            'idUser' => '3',
            'idRol' => '4',
            'img_user' => 'supervisor.jpg',
            'telefono_user' => '5',
            'facebook_user' => 'polizaderentas',
            'bio_user' => 'Lorem, lorem'
        ]);
        DB::table('controls')->insert([
            'idSucursal' => '0',
            'idUser' => '4',
            'idRol' => '5',
            'img_user' => 'admin.jpg',
            'telefono_user' => '5',
            'facebook_user' => 'polizaderentas',
            'bio_user' => 'Lorem, lorem'
        ]);
        DB::table('controls')->insert([
            'idSucursal' => '3',
            'idUser' => '5',
            'idRol' => '1',
            'img_user' => 'octavior.jpg',
            'telefono_user' => '427 121 1131',
            'facebook_user' => 'polizaderentas',
            'bio_user' => 'Octavio Ramírez Sánchez abogado civil, familiar y mercantil. Especialista en orientar y dar apoyo a clientes en el área de México, Querétaro y sus alrededores.
            Octavio nació en la Ciudad de México en 1970. Estudio en la Universidad Autónoma de México, terminando sus estudios en el año 2007 en el Colegio Humanitas con una maestría por un año en Derecho Corporativo.
            La experiencia laboral de Octavio ha sido dentro de la rama de la abogacía, se inicia en el Tribunal Superior de Justicia, como auxiliar del juzgado 19º. Civil, tiempo después comienza a laborar para una firma privada de abogados mientras continuaba con sus estudios en la Universidad, formó parte del área jurídica del PRI (Partido Revolucionario Institucional), durante seis meses formó parte del equipo del IFE (Instituto Federal Electoral) ahora INE, en el año 2000 para las elecciones presidenciales, una vez que terminó el proyecto en dicha institución se unió al del Tribunal Superior de Justicia en la Ciudad de México, concluyendo su relación laboral como proyectista en el juzgado 6º. Civil en el año 2009.
            En el año 2009 comienza a trabajar por su cuenta en San Juan del Río, Querétaro, litigando y asesorando clientes en las materias civil, mercantil y familiar llevando a cabo casos en la Ciudad de México, San Juan del Río y Querétaro según los clientes lo requieran.
            Actualmente se encuentra cursando una certificación para Administrador profesional de Condominios y en espera de su certificación como Asesor Inmobiliario.

            '
        ]);
        DB::table('controls')->insert([
            'idSucursal' => '3',
            'idUser' => '6',
            'idRol' => '1',
            'img_user' => 'lourdesr.jpg',
            'telefono_user' => '442-780-2959',
            'facebook_user' => 'polizaderentas',
            'bio_user' => 'Lourdes Sánchez Domínguez; abogada de profesión se desempeña en las áreas: civil, familiar y agrario, orientación y asesoría jurídica a clientes con situaciones legales en conflicto.
            Lourdes Sánchez Domínguez, nació en la ciudad de Córdoba Veracruz, en el año de 1973, estudió en la Universidad Veracruzana, trabajó en Banco Inverlat en labores como auxiliar de recursos humanos, pasando después al departamento administrativo, gestión, y cobranza de crédito, en control interno, remesas, cartera, crédito, posteriormente mostrador múltiple, finalizando como ejecutivo de cuenta de 1990 a 1997; DESPACHO JURÍDICO despacho jurídico a cargo del licenciado Lázaro Fernández García en el cual mi actividad principal era como litigante, 1998 – 1999 BANCO BANAMEX Mi desempeño fue como Ejecutivo de Servicio (Ventas, colocación, créditos) atención y servicio al cliente dentro y fuera de sucursal. Y de manera mensual brindaba capacitación de “Calidad en el Servicio” a los nuevos compañeros. 2001 – 2005, SERVICIOS ESPECIALIZADOS EN RECURSOS HUMANOS Inicio desempeñándome como Ejecutivo de Ventas, asistiendo a los instructores en sus capacitaciones y posteriormente en el departamento de reclutamiento y selección. 2005 – 2007; AVANZA CONSULTORES Somos una firma administrativa y legal, conformada por un selecto grupo de asesores especializados en proporcionar soluciones y mejoras a su empresa o negocio mediante la consultoría en el área de Ventas, Calidad en el Servicio, Desarrollo Humano y capacitación para el personal de la empresa en diversas áreas, según necesidades y requerimientos 2008-2010. En avanza consultores desempeño la coordinación de la consultoría de marzo 2008 a noviembre de 2010. NOTARIA PUBLICA 13 Específicamente en el departamento jurídico relativo a bienes y patrimonio, procesos relacionados a la regulación de la propiedad según la figura jurídica necesaria, 2011 – 2016. INSTITUTO CORDOBÉS DE LAS MUJERES POR LA IGUALDAD me desempeñe en el área jurídica del Instituto Cordobés de las Mujeres por la Igualdad, con participación activa en el departamento de mediación, además de dar a conocer los derechos de las mujeres, cursos con distintos temas que abonan al empoderamiento de la mujer, 2017 DESPACHO JURÍDICO “MOYA VARGAS Y ASOCIADOS” Tengo a mi cargo ventilar los asuntos civiles, familiares y agrarios con el objeto de regularizar la situación jurídica de bienes y personas, 2018 – a 2020.
            '
        ]);
        DB::table('controls')->insert([
            'idSucursal' => '3',
            'idUser' => '7',
            'idRol' => '1',
            'img_user' => 'alejandrom.jpg',
            'telefono_user' => '55-7988-6502',
            'facebook_user' => 'polizaderentas',
            'bio_user' => 'Alejandro Mariaca es especialista en asesoría jurídica privada y consultoría empresarial, ha colaboro durante 6 años en el sector gubernamental, 8 años en el sector de Tecnología y Telecomunicaciones, 7 años en la industria farmacéutica y 8 años patrocinando juicios en las materias civil, mercantil, penal, administrativa, amparo y laboral.
            Cuenta con amplia experiencia en recuperación de bienes, cartera vencida, restructuras, estrategias fiscales y defensa en delitos patrimoniales y de alto impacto.
            Cuento con un Grado de Maestría en Juicios Orales, diplomados en Finanzas, Administración, Gestión de Negocios,  Estrategias Comerciales, actualmente concluyendo el  grado de Maestría en  el Juicio de Amparo y el Grado de Maestría en Derecho Civil.
            '
        ]);


    }
}
