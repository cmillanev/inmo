<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([
            'name' => 'Rebeca Sandra García González',
            'email' => 'sandra@polizarentas.com',
            'password' => Hash::make('password'),

        ]);
        DB::table('users')->insert([
            'name' => 'Fernanda Vergara González',
            'email' => 'fernanda@polizarentas.com',
            'password' => Hash::make('password'),

        ]);

        DB::table('users')->insert([
            'name' => 'Supervisor',
            'email' => 'supervisor@polizarentas.com',
            'password' => Hash::make('password'),

        ]);
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@polizarentas.com',
            'password' => Hash::make('password'),

        ]);
        DB::table('users')->insert([
            'name' => 'Octavio Ramírez Sánchez',
            'email' => 'oramirez@polizaderentas.com',
            'password' => Hash::make('password'),

        ]);
        DB::table('users')->insert([
            'name' => 'Lourdes Sánchez Domínguez',
            'email' => 'lsanchez@polizaderentas.com',
            'password' => Hash::make('password'),

        ]);
        DB::table('users')->insert([
            'name' => 'Alejandro Marica Melendez',
            'email' => 'amariaca@polizaderentas.com',
            'password' => Hash::make('password'),

        ]);


    }
}
