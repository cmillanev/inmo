<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'nombre_rol' => 'Abogado Jr',   
        ]);
        DB::table('roles')->insert([
            'nombre_rol' => 'Abogado',   
        ]);
        DB::table('roles')->insert([
            'nombre_rol' => 'Gerente',   
        ]);
        DB::table('roles')->insert([
            'nombre_rol' => 'Supervisor',   
        ]);
        DB::table('roles')->insert([
            'nombre_rol' => 'Administrador',   
        ]);
       

       
    }
}
