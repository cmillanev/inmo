<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CalculationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('calculations')->insert([
        'limInferior' => '0.01',
        'limSuperior' => '1735.56',
        'cuotaFija' => '0',
        'porciento' => '1.92'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '1735.57',
        'limSuperior' => '14730.54',
        'cuotaFija' => '33.33',
        'porciento' => '6.4'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '14730.55',
        'limSuperior' => '25887.6',
        'cuotaFija' => '864.99',
        'porciento' => '10.88'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '25887.61',
        'limSuperior' => '30093.21',
        'cuotaFija' => '2078.88',
        'porciento' => '16'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '30093.22',
        'limSuperior' => '36029.82',
        'cuotaFija' => '2751.78',
        'porciento' => '17.92'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '36029.83',
        'limSuperior' => '72666.93',
        'cuotaFija' => '3815.61',
        'porciento' => '21.36'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '72666.94',
        'limSuperior' => '114533.07',
        'cuotaFija' => '11641.32',
        'porciento' => '23.52'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '114533.08',
        'limSuperior' => '218662.5',
        'cuotaFija' => '21488.22',
        'porciento' => '30'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '218662.5',  
        'limSuperior' => '291449.99',
        'cuotaFija' => '52727.07',
        'porciento' => '32'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '291550',
        'limSuperior' => '874650',
        'cuotaFija' => '76051.05',
        'porciento' => '34'
     ]);
     DB::table('calculations')->insert([
        'limInferior' => '874650.01', 
        'limSuperior' => '999999999',
        'cuotaFija' => '274305.06',
        'porciento' => '35'
     ]);
    }
}
