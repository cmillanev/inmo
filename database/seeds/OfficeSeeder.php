<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offices')->insert([
            'nombre_suc' => 'Póliza de Rentas Metepec',
            'codigo' => 'PolRenMet',
            'calle' => 'Pedro Asencio',
            'numInt' => '3',
            'numExt' => '313',
            'colonia' => 'Barrio de San Mateo',
            'municipio' => 'Metepec',
            'estado' => 'México',
            'cp' => '0',
            'email_suc' => 'sandra@polizaderesntas.com',
            'telefono_suc' => '7224024459',
            'img_suc' => 'metepec.jpg',
            'descripcion_suc' => 'Somos una empresa dedicada a proteger el patrimonio de las personas, nuestros servicios están destinados a propietarios, inmobiliarias o administradores de inmuebles que buscan dar sus propiedades en arrendamiento. Estamos comprometidos a ofrecer a nuestros clientes protección total a su patrimonio, y a su negocio. Cobrando de manera justa únicamente lo que cada cliente necesita. En póliza de Rentas encontrarás toda la protección a un único precio.',
            'lat' => '19.2558185',
            'lng' => '-99.5934578'
        ]);
        DB::table('offices')->insert([
            'nombre_suc' => 'Póliza de Rentas Ciudad de México',
            'codigo' => 'POLRENTCDMX',
            'calle' => 'Lago Alberto',
            'numInt' => '.',
            'numExt' => '375',
            'colonia' => 'Anahuac I Sección',
            'municipio' => 'CDMX',
            'estado' => 'CDMX',
            'cp' => '36567',
            'email_suc' => 'fernanda@polizarentas.com',
            'telefono_suc' => '34345465',
            'img_suc' => 'cdmx.jpg',
            'descripcion_suc' => 'Somos una empresa dedicada a proteger el patrimonio de las personas, nuestros servicios están destinados a propietarios, inmobiliarias o administradores de inmuebles que buscan dar sus propiedades en arrendamiento. Estamos comprometidos a ofrecer a nuestros clientes protección total a su patrimonio, y a su negocio. Cobrando de manera justa únicamente lo que cada cliente necesita. En póliza de Rentas encontrarás toda la protección a un único precio.',
            'lat' => '19.438537',
            'lng' => '-99.1809717'
        ]);
        DB::table('offices')->insert([
            'nombre_suc' => 'Póliza de Rentas Querétaro',
            'codigo' => 'POLRENTQUER',
            'calle' => 'Circuito Álamos',
            'numInt' => '.',
            'numExt' => '83',
            'colonia' => 'Álamos 2ª sección',
            'municipio' => 'Querétaro',
            'estado' => 'Querétaro',
            'cp' => '76160',
            'email_suc' => 'alegil@polizaderentas.com',
            'telefono_suc' => '55-5214-7989',
            'img_suc' => 'cdmx.jpg',
            'descripcion_suc' => 'Somos una empresa dedicada a proteger el patrimonio de las personas, nuestros servicios están destinados a propietarios, inmobiliarias o administradores de inmuebles que buscan dar sus propiedades en arrendamiento. Estamos comprometidos a ofrecer a nuestros clientes protección total a su patrimonio, y a su negocio. Cobrando de manera justa únicamente lo que cada cliente necesita. En póliza de Rentas encontrarás toda la protección a un único precio.',
            'lat' => '19.438537',
            'lng' => '-99.1809717'
        ]);

    }
}
