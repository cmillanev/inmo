<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login ');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('offices', 'OfficesController');
Route::resource('users', 'UsersController');
Route::resource('files', 'FilesController');
Route::get('files/{id}email', 'FilesController@email');
Route::resource('online', 'OnlineController');
Route::resource('calculations', 'CalculationsController');
Route::resource('expedientes', 'ExpedienteController');

Route::resource('calculadora', 'CalculadoraController');
Route::post('calculadora', 'CalculadoraController@calculo')->name('calculo.post');


Route::get('mapa', function () {
    return view('formulario.mapa ');
});

Route::resource('directorio', 'DirectorioController');

Route::resource('sucursales', 'SucursalesController');
Route::post('sucursales', 'SucursalesController@sucursal')->name('sucursal.post');

