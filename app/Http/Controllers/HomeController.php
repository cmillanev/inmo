<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

     $idUser = Auth::User()->id;   
     
     $idSucursal = DB::select('SELECT idSucursal, idRol FROM controls WHERE idUser = ?', [$idUser]);
     $idRol = $idSucursal[0]->idRol;
     $idSuc = $idSucursal[0]->idSucursal;
     if ($idRol == 1 || $idRol == 2 || $idRol == 3){

    
        $nuevas =  DB::table('cpolicies')
        ->select('idPer')
        ->where('status',  '1' && 'idSuc',  $idSuc)
        ->count();
  
         $docu =  DB::table('cpolicies')
         ->select('idPer')
        ->where('status',  '2' && 'idSuc',  $idSuc)
        ->count();  
  
        $revi =  DB::table('cpolicies')
        ->select('idPer')
        ->where('status',  '3' && 'idSuc',  $idSuc)
        ->count();  
  
        $aprobada =  DB::table('cpolicies')
        ->select('idPer')
        ->where('status',  '4' && 'idSuc',  $idSuc)
        ->count();  
        
        $rechazada =  DB::table('cpolicies')
        ->select('idPer')
        ->where('status',  '5' && 'idSuc',  $idSuc)
        ->count();  
      
         

     }else{
          $nuevas =  DB::table('cpolicies')
      ->where('status', '=', '1')
      ->count();

       $docu =  DB::table('cpolicies')
      ->where('status', '=', '2')
      ->count();  

      $revi =  DB::table('cpolicies')
      ->where('status', '=', '3')
      ->count();  

      $aprobada =  DB::table('cpolicies')
      ->where('status', '=', '4')
      ->count();  
      
      $rechazada =  DB::table('cpolicies')
      ->where('status', '=', '5')
      ->count();    
     }
    
       
 

     


        return view('home', compact('nuevas', 'docu', 'revi', 'aprobada', 'rechazada', 'idRol'));

    }
}
