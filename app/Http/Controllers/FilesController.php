<?php

namespace App\Http\Controllers;

use App\Mail\Revision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\FacadesDB;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function index()
    {

         //CONSULTA ID DEL USUARIO

        $idUser = Auth::user()->id;



         //Selecciona el ID de la sucursal a la que pertenece el usuario

         $idSucursal = DB::select('SELECT idSucursal, idRol FROM controls WHERE idUser = ?', [$idUser]);

         $idRol = $idSucursal[0]->idRol;
         $idSuc = $idSucursal[0]->idSucursal;
        if ($idRol == 1 || $idRol == 2 || $idRol == 3){
            $files = DB::table('personals')
            ->join('cpolicies', 'personals.id', '=', 'cpolicies.idPer')
            ->join('polizas', 'personals.id', '=', 'polizas.idPer')
            ->join('offices', 'cpolicies.idSuc', '=', 'offices.id')
            ->select('polizas.*','personals.id','personals.nombres', 'personals.apellidoP',
             'personals.apellidoM', 'personals.email', 'personals.celular',
             'cpolicies.idSuc', 'cpolicies.status', 'offices.nombre_suc')
             ->where('cpolicies.idSuc', '=', $idSuc)
            ->get();

        }else {

            $files = DB::table('personals')
            ->join('cpolicies', 'personals.id', '=', 'cpolicies.idPer')
            ->join('polizas', 'personals.id', '=', 'polizas.idPer')
            ->join('offices', 'cpolicies.idSuc', '=', 'offices.id')
            ->select('polizas.*','personals.id','personals.nombres', 'personals.apellidoP',
             'personals.apellidoM', 'personals.email', 'personals.celular',
             'cpolicies.idSuc', 'cpolicies.status', 'offices.nombre_suc')
            ->get();

        }




        return view('files.index', compact('files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        


        return view('files.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Comentarios



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        

        $idUser = Auth::User()->id;   

        $file = DB::table('personals')
        ->join('cpolicies', 'personals.id', '=', 'cpolicies.idPer')
        ->join('polizas', 'personals.id', '=', 'polizas.idPer')
        ->join('documents', 'personals.id', '=', 'documents.idPer')
        ->select('polizas.*','personals.id','personals.nombres', 'personals.apellidoP',
         'personals.apellidoM', 'personals.email', 'personals.celular',
         'cpolicies.idSuc', 'cpolicies.status', 'documents.*')
         ->where('personals.id', '=', $id)
        ->get();



        return view('files.show', compact('file', 'idUser'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $personals = DB::table('personals')
        ->where('id', '=', $id)
        ->get();

        $jobs = DB::table('jobs')
        ->where('id', '=', $id)
        ->get();

        $ref = DB::table('references')
        ->where('id', '=', $id)
        ->get();

        $fiador = DB::table('fiadores')
        ->where('id', '=', $id)
        ->get();

        $document = DB::table('documents')
        ->where('id', '=', $id)
        ->get();

        return view('files.edit', compact('personals', 'jobs', 'ref', 'fiador', 'document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //



     // return request('control');

       switch (request('control')) {
           case 'personals':

            DB::table('personals')
            ->where('id', $id )
            ->update([
                'apellidoP' => request('apellidoP'),
                'apellidoM' => request('apellidoM'),
                'nombres'   => request('nombres'),
                'nacionalidad' => request('nacionalidad'),
                'especifiqueN' => request('especifiqueN'),
                'sexo' => request('sexo'),
                'edoCivil' => request('edoCivil'),
                'email' => request('email'),
                'iden' => request('iden'),
                'ine' => request('ine'),
                'fechaNac' => request('fechaNac'),
                'rfc' => request('rfc'),
                'curp' => request('curp'),
                'telefonoP' => request('telefonoP'),
                'celular'   => request('celular'),
                'apellidoPc'       => request('apellidoPc'),
                'apellidoMc' => request('apellidoMc'),
                'nombrec' => request('nombrec'),
                'telefonoc' => request('telefonoc'),
                'adultos' => request('adultos'),
                'menores' => request('menores'),
                'cuantos' => request('cuantos'),
                'situacion' => request('situacion'),
                'motivo' => request('motivo'),
                'mascotas' => request('mascotas'),
                'especifique' => request('especifique'),
                'calle' => request('calle'),
                'numInt' => request('numInt'),
                'numExt' => request('numExt'),
                'cp' => request('cp'),
                'colonia' => request('colonia'),
                'mun' => request('mun'),
                'estado' => request('estado'),
                'apellidoPa' => request('apellidoPa'),
                'apellidoMa' => request('apellidoMa'),
                'nombrea' => request('nombrea'),
                'telefonoa' => request('telefonoa'),
                'renta' => request('renta'),
                'anioRenta' => request('anioRenta'),
                'nombreEmp' => request('nombreEmp'),
                'apellidoPe' => request('apellidoPe'),
                'apellidoMe' => request('apellidoMe'),
                'nombreRazon' => request('nombreRazon'),
                'escritura' => request('escritura'),
                'notario' => request('notario'),
                'fechaConst' => request('fechaConst'),
                'giro' => request('giro'),
                'telefonoE' => request('telefonoE'),
                'extE' => request('extE'),
                'emailE' => request('emailE'),
                'nombreF' => request('nombreF'),
                'correoF' => request('correoF'),
                'dirFisc' => request('dirFisc'),
                'rfcfis' => request('rfcfis'),
                'calleF' => request('calleF'),
                'numIntF' => request('numIntF'),
                'numExtF' => request('numExtF'),
                'cpF' => request('cpF'),
                'coloniaF' => request('coloniaF'),
                'munF' => request('munF'),
                'estadoF' => request('estadoF')
                ]);

                DB::table('cpolicies')
                ->where('idPer', $id)
                ->update([
                    'status' => request('status')
                ]);

               break;

               case 'jobs':
                # code...
                  DB::table('jobs')
                ->where('idPer', $id)
                ->update([
                    'profesion' => request('profesion'),
                    'tipoEmp' => request('tipoEmp'),
                    'tel' => request('tel'),
                    'ext' => request('ext'),
                    'empresa' => request('empresa'),
                    'calle' => request('calle'),
                    'numExt' => request('numExt'),
                    'numInt' => request('numInt'),
                    'cp' => request('cp'),
                    'colonia' => request('colonia'),
                    'mun' => request('mun'),
                    'estado' => request('estado'),
                    'fechaIng' => request('fechaIng'),
                    'apellidoPj' => request('apellidoPj'),
                    'apellidoMj' => request('apellidoMj'),
                    'nombrej' => request('nombrej'),
                    'telefonoj' => request('telefonoj'),
                    'extj' => request('extj'),
                    'ingresos' => request('ingresos'),
                    'ingresoFam' => request('ingresoFam'),
                    'numPerIngre' => request('numPerIngre'),
                    'numPerDep' => request('numPerDep'),
                    'apellidoPa' => request('apellidoPa'),
                    'apellidoMa' => request('apellidoMa'),
                    'nombrea' => request('nombrea'),
                    'parentesco' => request('parentesco'),
                    'telefonoA' => request('telefonoA'),
                    'empresaA' => request('empresaA'),
                    'ingresoA' => request('ingresoA')

                ]);

                break;
            case 'references':

                DB::table('references')
                ->where('id', '=', $id)
                ->update([
                    'apellidoPr1' => request('apellidoPr1'),
                    'apellidoMr1' => request('apellidoMr1'),
                    'nombrer1' => request('nombrer1'),
                    'relacionr1' => request('relacionr1'),
                    'telefonor1' => request('telefonor1'),
                    'apellidoPr2' => request('apellidoPr2'),
                    'apellidoMr2' => request('apellidoMr2'),
                    'nombrer2' => request('nombrer2'),
                    'relacionr2' => request('relacionr2'),
                    'telefonor2' => request('telefonor2'),
                    'apellidoPf1' => request('apellidoPf1'),
                    'apellidoMf1' => request('apellidoMf1'),
                    'nombref1' => request('nombref1'),
                    'relacionf1' => request('relacionf1'),
                    'telefonof1' => request('telefonof1'),
                    'apellidoPf2' => request('apellidoPr1'),
                    'apellidoMf2' => request('apellidoMf2'),
                    'nombref2' => request('nombref2'),
                    'relacionf2' => request('relacionf2'),
                    'telefonof2' => request('telefonof2'),
                ]);

            break;

           case 'fiadores':

            DB::table('fiadores')
            ->where('id', $id)
            ->update([
                    'apellidoPf' => request('apellidoPf'),
                    'apellidoMf' => request('apellidoMf'),
                    'nombref' => request('nombref'),
                    'telefonof' => request('telefonof'),
                    'celularf' => request('celularf'),
                    'relacionf' => request('relacionf'),
                    'tiempof' => request('tiempof'),
                    'nacionalidadf' => request('nacionalidadf'),
                    'especifiquef' => request('especifiquef'),
                    'sexof' => request('sexof'),
                    'edoCivilf' => request('edoCivilf'),
                    'emailf' => request('emailf'),
                    'identif' => request('identif'),
                    'inef' => request('inef'),
                    'fechaNacf' => request('fechaNacf'),
                    'rfcf' => request('rfcf'),
                    'curpf' => request('curpf'),
                    'callef' => request('callef'),
                    'numExtf' => request('numExtf'),
                    'numIntf' => request('numIntf'),
                    'cpf' => request('cpf'),
                    'coloniaf' => request('coloniaf'),
                    'munif' => request('munif'),
                    'estadof' => request('estadof'),
                    'profesionf' => request('profesionf'),
                    'tipoEmpleof' => request('tipoEmpleof'),
                    'telefonoEf' => request('telefonoEf'),
                    'extEf' => request('extEf'),
                    'empresaf' => request('empresaf'),
                    'calleEf' => request('calleEf'),
                    'nunExtEf' => request('numExtEf'),
                    'numIntEf' => request('numIntEf'),
                    'cpEf' => request('cpEf'),
                    'coloniaEf' => request('coloniaEf'),
                    'muniEf' => request('muniEf'),
                    'estadoEf' => request('estadoEf'),
                    'ingresof' => request('ingresof'),
                    'fechaIngf' => request('fechaIngf'),
                    'calleDf' => request('calleDf'),
                    'numExtDf' => request('numExDf'),
                    'numIntDf' => request('numIntDf'),
                    'cpDf' => request('cpDf'),
                    'coloniaDf' => request('coloniaDf'),
                    'muniDf' => request('muniDf'),
                    'estadoDf' => request('estadoDf'),
                    'escrituraf' => request('escrituraf'),
                    'foliof' => request('foliof'),
                    'notariaf' => request('notariaf'),
                    'fechaf' => request('fechaf'),
                    'boletaf' => request('boletaf'),
                    'autorizo' => request('autorizo'),
                    'declaro' => request('declaro')
            ]);

        break;

            case 'terminado':

                DB::table('cpolicies')
                ->where('idPer', $id)
                ->update([
                    'status' => request('status')
                ]);

                //Aqui el email de revisión

                $datosEmail = DB::table('personals')
                ->where('id', '=', $id)
                ->get();


                $msg = $datosEmail[0]->nombres;
                $email = $datosEmail[0]->email;


             //   Mail::to($email)->queue(new Revision($msg, $email));
            


            break;
            case 'documents':



               if($request->hasFile('identificacion')){


                   $fileIden = $request->file('identificacion');
                   $iden = time().$fileIden->getClientOriginalName();

                   $fileIden->move(public_path().'/images/', $iden);


               }

               if($request->hasFile('domicilio')){


                $fileDom = $request->file('domicilio');
                $dom = time().$fileDom->getClientOriginalName();

                $fileDom->move(public_path().'/images/', $dom);


            }else {}

            if($request->hasFile('ingresos')){


                $fileIng = $request->file('ingresos');
                $ing = time().$fileIng->getClientOriginalName();

                $fileIng->move(public_path().'/images/', $ing);


            }

                DB::table('documents')
                ->where('idPer', $id)
                ->update([
                    'identificacion' => $iden,
                    'domicilio' => $dom,
                    'ingresos' => $ing
                ]);

            break;


       }


        return redirect('/files');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function email($id){

        return 'Hola mundo';

    }
}
