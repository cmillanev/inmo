<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Office;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\ElseIf_;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //

        $idUser = Auth::User()->id;

        $idSucursal = DB::select('SELECT idSucursal, idRol FROM controls WHERE idUser = ?', [$idUser]);
        $idRol = $idSucursal[0]->idRol;
        $idSuc = $idSucursal[0]->idSucursal;



        if ($idRol == 3){

            $users = DB::table('users')
                    ->leftJoin('controls', 'users.id', '=', 'controls.idUser')
                    ->leftJoin('offices', 'controls.idSucursal', '=', 'offices.id')
                    ->join('roles', 'controls.idRol', '=', 'roles.id')
                    ->where('controls.idSucursal', '=', $idSuc)
                    ->get();
        }

        if ($idRol == 1 || $idRol == 2){

            $users = DB::table('users')
                    ->leftJoin('controls', 'users.id', '=', 'controls.idUser')
                    ->leftJoin('offices', 'controls.idSucursal', '=', 'offices.id')
                    ->join('roles', 'controls.idRol', '=', 'roles.id')
                    ->where('users.id', '=', $idUser)
                    ->get();

         }

        if ($idRol == 4 || $idRol == 5){
            $users = DB::table('users')
            ->leftJoin('controls', 'users.id', '=', 'controls.idUser')
            ->leftJoin('offices', 'controls.idSucursal', '=', 'offices.id')
            ->join('roles', 'controls.idRol', '=', 'roles.id')
            ->get();
        }


            return view ('users.index', compact('users'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Id user
        $idUser = Auth::User()->id;
        $idSucursal = DB::select('SELECT idSucursal, idRol FROM controls WHERE idUser = ?', [$idUser]);
        //Id rol
        $idRol = $idSucursal[0]->idRol;
        //Id suc
        $idSuc = $idSucursal[0]->idSucursal;

    // Si es  admin o sup selecciona todos los roles y sucursales
        if($idRol == 4 || $idRol == 5){

            $offices = Office::get();
            $roles = Role::get();

        }
        //Si es gerente selecciona solo su sucursal y roles inferiores
        if( $idRol == 3 ){

            $roles = DB::select('SELECT id, nombre_rol FROM roles WHERE id <= 2 ');
            $offices = DB::select('SELECT id, nombre_suc from offices where id = ?', [$idSuc]);

        }



        return view('users.create', compact('offices', 'roles', 'idUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $idSucursal = $request->idSucursal;
        $idRol = $request->rol;
        //$urlwp->urlwp = $request->urlwp;

        if ($user->save()){


        $idUsers = DB::select('SELECT MAX(id) AS idUser FROM users');

          //INSERTA EN LA TABLA USERSUCURSAL EL ID DE USUARIO Y SUCURSAL
          DB::table('controls')->insert([
            'idSucursal'=> $idSucursal,
            'idUser'=> $idUsers[0]->idUser,
            'idRol'=> $idRol,
            'img_user' => $request->img_user,
            'telefono_user' => $request->telefono_user,
            'facebook_user' => $request->facebook_user,
            'bio_user' => $request->bio_user,
            'whatsapp' => $request->whatsapp,
            'anio_exp' => $request->anio_exp,
            'linkedIn' => $request->linkedIn,
            'bio_user' => $request->bio_user

            ]);


       return redirect('/users');

          }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $users = DB::table('users')
        ->leftJoin('controls', 'users.id', '=', 'controls.idUser')
        ->leftJoin('offices', 'controls.idSucursal', '=', 'offices.id')
        ->join('roles', 'controls.idRol', '=', 'roles.id')
        ->where('users.id', '=', $id)
        ->get();

        $idUser = Auth::User()->id;
        $idSucursal = DB::select('SELECT idSucursal, idRol FROM controls WHERE idUser = ?', [$idUser]);
         //Id rol
         $idRol = $idSucursal[0]->idRol;
         //Id suc
         $idSuc = $idSucursal[0]->idSucursal;


    // Si es  admin o sup selecciona todos los roles y sucursales
        if($idRol == 4 || $idRol == 5){

            $offices = Office::get();
            $roles = Role::get();

        }

        if( $idRol == 3 ){

            $roles = DB::select('SELECT id, nombre_rol FROM roles WHERE id <= 2 ');
            $offices = DB::select('SELECT id, nombre_suc from offices where id = ?', [$idSuc]);

        }
        if( $idRol == 1 || $idRol == 2  ){

            $roles = DB::select('SELECT id, nombre_rol FROM roles WHERE id = ? ', [$idRol]);
            $offices = DB::select('SELECT id, nombre_suc from offices where id = ?', [$idSuc]);

        }

        return view('users.edit', compact('users', 'roles', 'offices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //



        $user = User::findOrFail($id);

        $user->name = $request->name;


        if ( $request->password != null ){

            $user->password = $request->password;
        }

        $user->save();


        DB::table('controls')
                ->where('idUser', $id)
                ->update([
                    'idSucursal'=> $request->idSucursal,
                    'idRol'=> $request->rol,
                    
                    'telefono_user' => $request->telefono_user,
                    'facebook_user' => $request->facebook_user,
                    'bio_user' => $request->bio_user,
                    'whatsapp' => $request->whatsapp,
                    'anio_exp' => $request->anio_exp,
                    'linkedIn' => $request->linkedIn,
                    'bio_user' => $request->bio_user
                ]);

        return  redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $user = User::findOrFail($id);

        if ( $user ->delete()){
            return redirect('/users');
        }else{
            return response()->json([
                'mensaje'=> 'Error al eliminar el usuario'
            ]);
        }
    }
}
