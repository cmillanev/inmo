<?php

namespace App\Http\Controllers;

use App\Mail\NewFile;
use App\Office;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ExpedienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $offices = Office::get();
        $users = User::all();
        
        return view ('expedientes.create', compact('offices', 'users'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     
        $hoy = date("Y-m-d");
        $msg = request('nombres');
        $email = request('email');


        Mail::to($email)->queue(new NewFile($msg, $email));

    

       // return $request;
        DB::table('personals')->insert([
            'apellidoP'=>request('apellidoP'),
            'apellidoM'=>request('apellidoM'),
            'nombres'=>request('nombres'),
            'email'=>request('email'),
            'celular'=>request('celular'),
            'fecha_ini'=>request($hoy)

        ]);



        $idPer = DB::select('SELECT MAX(id) AS idPer FROM personals'); 

      

        DB::table('jobs')->insert([

            'idPer'=>$idPer[0]->idPer
           

        ]);

        DB::table('references')->insert([

            'idPer'=>$idPer[0]->idPer
           

        ]);  

         DB::table('fiadores')->insert([

            'idPer'=>$idPer[0]->idPer
            

        ]); 

        DB::table('documents')->insert([

            'idPer'=>$idPer[0]->idPer
            

        ]); 

        
        //GUARDA EN CPOLICES ID DE PERSONALES, ID USER, ID STATUS
        DB::table('cpolicies')->insert([

            'idPer'=>$idPer[0]->idPer,
            'idUser'=>request('idAsesor'),
            'status'=>request('status'),
            'idSuc'=>request('idSucursal'),
            'fecha_ini' => $hoy

        ]);

        $idSuc = request('idSucursal');

        //Genera la clave de la póliza

        $claveSuc = DB::select('SELECT codigo FROM offices WHERE id = ?', [$idSuc]);
        $genClave = $claveSuc[0]->codigo;
        $claveFinal = $genClave . '0001';
        //Guarda datos de la póliza 
        DB::table('polizas')->insert([
            'idPer'=>$idPer[0]->idPer,
           
            'clave'=>$claveFinal,
            'fecha_ini'=>$hoy
        ]);

    

       return redirect('/expedientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
