<?php

namespace App\Http\Controllers;

use App\Db7_form;
use App\Mail\NewFile;
use App\Office;
use App\Online;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Mail;
use App\User;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OnlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
/*
 $db7_form = Db7_form::all();

        $decode = json_decode($db7_form);

        $db7_form[8]['form_value'];

        $array = explode(";s:", $db7_form[7]['form_value']);

         $array[11];

        $url = explode('"', $array[11]);

       $url[1];
*/


       $datos = Online::all();

        return view('online.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $idUser = Auth::User()->id;

        $idSucursal = DB::select('SELECT idSucursal, idRol FROM controls WHERE idUser = ?', [$idUser]);
        $idRol = $idSucursal[0]->idRol;
        $idSuc = $idSucursal[0]->idSucursal;
        if ($idRol == 3){

            $users = DB::table('users')
                    ->leftJoin('controls', 'users.id', '=', 'controls.idUser')
                    ->leftJoin('offices', 'controls.idSucursal', '=', 'offices.id')
                    ->join('roles', 'controls.idRol', '=', 'roles.id')
                    ->where('controls.idSucursal', '=', $idSuc)
                    ->get();

            $offices = DB::table('offices')
                    ->where('id', $idSuc )
                    ->get();

        }

        if ($idRol == 1 || $idRol == 2){

                  $users = DB::table('users')
                    ->leftJoin('controls', 'users.id', '=', 'controls.idUser')
                    ->leftJoin('offices', 'controls.idSucursal', '=', 'offices.id')
                    ->join('roles', 'controls.idRol', '=', 'roles.id')
                    ->where('users.id', '=', $idUser)
                    ->get();

                    $offices = DB::table('offices')
                                ->where('id', $idSuc )
                                ->get();

         }

        if ($idRol == 4 || $idRol == 5){
            $users = DB::table('users')
            ->leftJoin('controls', 'users.id', '=', 'controls.idUser')
            ->leftJoin('offices', 'controls.idSucursal', '=', 'offices.id')
            ->join('roles', 'controls.idRol', '=', 'roles.id')
            ->get();
            $offices = Office::get();
        }




        return view ('online.create', compact('offices', 'users'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $hoy = date("Y-m-d");
        $msg = request('nombres');
        $email = request('email');


        Mail::to($email)->queue(new NewFile($msg, $email));



       // return $request;
        DB::table('personals')->insert([
            'apellidoP'=>request('apellidoP'),
            'apellidoM'=>request('apellidoM'),
            'nombres'=>request('nombres'),
            'email'=>request('email'),
            'celular'=>request('celular'),
            'fecha_ini'=>request($hoy)

        ]);



        $idPer = DB::select('SELECT MAX(id) AS idPer FROM personals');



        DB::table('jobs')->insert([

            'idPer'=>$idPer[0]->idPer


        ]);

        DB::table('references')->insert([

            'idPer'=>$idPer[0]->idPer


        ]);

         DB::table('fiadores')->insert([

            'idPer'=>$idPer[0]->idPer


        ]);

        DB::table('documents')->insert([

            'idPer'=>$idPer[0]->idPer


        ]);

        //CONSULTA ID USER

        $idUser = Auth::user()->id;
        //Selecciona el ID de la sucursal a la que pertenece el usuario

        $idSucursal = DB::select('SELECT idSucursal FROM controls WHERE idUser = ?', [$idUser]);

        $idQuery = $idSucursal[0]->idSucursal;
        //GUARDA EN CPOLICES ID DE PERSONALES, ID USER, ID STATUS
        DB::table('cpolicies')->insert([

            'idPer'=>$idPer[0]->idPer,
            'idUser'=>request('idUser'),
            'status'=>request('status'),
            'idSuc'=>request('idSuc'),
            'fecha_ini' => $hoy

        ]);

        //Genera la clave de la póliza

        $claveSuc = DB::select('SELECT codigo FROM offices WHERE id = ?', [$idQuery]);
        $genClave = $claveSuc[0]->codigo;
        $claveFinal = $genClave . '0001';
        //Guarda datos de la póliza
        DB::table('polizas')->insert([
            'idPer'=>$idPer[0]->idPer,
            'renta'=>request('renta'),
            'montoPoliza'=>request('montoPoliza'),
            'calle'=>request('calle'),
            'numExt'=>request('numExt'),
            'numInt'=>request('numInt'),
            'referenciasUbicacion'=>request('referenciasUbicacion'),
            'ciudad'=>request('ciudad'),
            'estado'=>request('estado'),
            'lat'=>request('lat'),
            'lng'=>request('lng'),
            'clave'=>$claveFinal,
            'fecha_ini'=>$hoy
        ]);

       return redirect('/files');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
