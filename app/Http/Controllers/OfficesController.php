<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Office;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OfficesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $idUser = Auth::User()->id;

        $idSucursal = DB::select('SELECT idSucursal, idRol FROM controls WHERE idUser = ?', [$idUser]);
        $idRol = $idSucursal[0]->idRol;
        $idSuc = $idSucursal[0]->idSucursal;





        if ($idRol == 1 || $idRol == 2 || $idRol == 3){

            $offices = DB::table('offices')
                    ->leftJoin('controls', 'offices.id', '=', 'controls.idSucursal')
                    ->where('controls.idUser', '=', $idUser)
                    ->get();

         }

        if ($idRol == 4 || $idRol == 5){
            $offices = Office::get();

        }


        return view('offices.index', compact('offices', 'idRol'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('offices.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        return $request['img_ciudad'];


        if($request->hasFile('img_ciudad')){


            $fileCiudad = $request->file('img_ciudad');
            $imgCiudad = time().$fileCiudad->getClientOriginalName();

            $fileCiudad->move(public_path().'/img/sucursales/', $imgCiudad);

        }else{

            $imgCiudad = 'sinimagen.jpg';
        }

        if($request->hasFile('img_suc')){

            return 'entro';
            $fileSuc = $request->file('img_suc');
            $imgSuc = time().$fileSuc->getClientOriginalName();

            $fileSuc->move(public_path().'/img/sucursales/', $imgSuc);

        }else{
            $imgSuc = 'sinsuc.jpg';
        }

        Office::create([
            'nombre_suc' => request('nombre_suc'),
            'email_suc' => request('email_suc'),
            'codigo'=> request('codigo'),
            'calle'=> request('calle'),
            'numInt'=> request('numInt'),
            'numExt'=> request('numExt'),
            'colonia'=> request('colonia'),
            'municipio'=> request('municipio'),
            'estado'=> request('estado'),
            'cp'=> request('cp'),
            'telefono_suc'=> request('telefono_suc'),
            'lat'=> request('lat'),
            'lng'=> request('lng'),
            'img_ciudad' => $imgCiudad,
            'img_suc' => $imgSuc,
            'descripcion_suc' => request('descripcion_suc')
        ]);

        return redirect('/offices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sucursales = DB::table('offices')
        ->where('nombre_suc', '=', $id)
        ->get();

        return view('offices.show', compact('sucursales'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $office = Office::findOrFail($id);




        return view('offices.edit', compact('office'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $office = Office::findOrFail($id);

       DB::table('offices')
       ->where('id', $id)
       ->update([
       'nombre_suc' => request('nombre_suc'),
       'email_suc' => request('email_suc'),
       'codigo'=> request('codigo'),
       'calle'=> request('calle'),
       'numInt'=> request('numInt'),
       'numExt'=> request('numExt'),
       'colonia'=> request('colonia'),
       'municipio'=> request('municipio'),
       'estado'=> request('estado'),
       'cp'=> request('cp'),
       'telefono_suc'=> request('telefono_suc'),
       'lat'=> request('lat'),
       'lng'=> request('lng'),
       'img_ciudad' => 'img_ciudad',
       'img_suc' => 'img_suc',
       'descripcion_suc' => request('descripcion_suc')
       ]);


      return  redirect('/offices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
