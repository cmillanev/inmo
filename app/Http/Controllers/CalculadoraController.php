<?php

namespace App\Http\Controllers;

use App\Calculation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\Exists;
use Symfony\Component\Console\Input\Input;

class CalculadoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        
            return view('calculadora.index');
    
        
    }
    public function calculo(Request $request){

       $datos = Calculation::all();
        

        $input = $request->all();
         $renta = $_POST['name'];
         $deduccionCompro = 0;

         if($deduccionCompro != 0 ){

            $deduccionCiega = 0; 

         }else{
            $deduccionCiega = $renta * .35 ; 
         }

         $base = $renta - $deduccionCompro - $deduccionCiega;

         foreach ($datos as $dato) {
              
            if($base >= $dato->limInferior && $base <= $dato->limSuperior){
            $limiteInferior = $dato->limInferior;
            $impuesto = $dato->porciento;
            $cuotaFija = $dato->cuotaFija;
            
             
         }
     }
        $excedente = $base - $limiteInferior; 
        $impuestoMarginal = $excedente * .1088;
        $impuesto2 = $cuotaFija + $impuestoMarginal;

        $isrCargoMen = $impuesto2;


        
         
     
        
      //  return response()->json(['success'=>$name]);

      return view('calculadora.form.calculadora', compact('deduccionCiega',
       'base', 'limiteInferior', 'excedente', 'impuesto', 'impuestoMarginal',
        'cuotaFija', 'impuesto2', 'isrCargoMen' ));

    }

  
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('calculadora.show');
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
