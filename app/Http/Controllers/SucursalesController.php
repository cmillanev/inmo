<?php

namespace App\Http\Controllers;

use App\Office;
use DOMDocument;
use App\Mail\Contacto;
use App\Online;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Mail;
use Spatie\ArrayToXml\ArrayToXml;
class SucursalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *Todo esto viene de lo que se encuentra en Wordpress
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


         $sucursales = Office::get();





       return view('sucursales.index', compact('sucursales'));

    }

    public function sucursal(Request $request){




        $input = $request->all();
        //SI trae valor de formulario
        if (isset($_POST['idSucursal'])){

            $hoy = date("Y-m-d");

            $formulario = request('formulario');

            //Email del que abogado o sucursal
            $email = request('email');
            //Email del contacto
            $emailSuc = request('email_suc');
            //Nombre del contacto
            $nombre = request('nombres');
            //telefono
            $celular = request('celular');
            //Mensaje
            $msg =  request('mensaje');

            if ($formulario == 'abogado') {
                $emailSuc = request('emailUser');
            }

            //Se manda el Email para sucursal

             Mail::to($email)->queue(new Contacto($msg, $email, $emailSuc, $nombre, $celular, $hoy ));

             //Guardar los datos en la base de datos


            DB::table('onlines')->insert([
                'nombres'=>request('nombres'),
                'apellidoP'=>request('apellidoP'),
                'apellidoM'=>request('apellidoM'),
                'email'=>request('email'),
                'celular'=>request('celular'),
                'comentarios'=>$msg,
                'fecha_ini'=>$hoy

            ]);
                //Consulta el ultimo id de online
            $idOnline = DB::select('SELECT MAX(id) AS idOnline FROM onlines');
                //Guarda los datos en conlines (control de onlines)
            DB::table('conlines')->insert([
                'idSucursal'=>request('idSucursal'),
                'idUser'=>request('idUser'),
                'idOnline'=>$idOnline[0]->idOnline,
                'status'=>request('status'),
                'formulario'=>request('formulario'),
                'fecha_mod'=>$hoy

            ]);




             return redirect('/sucursales');

        }else{

     //Consulta
        $estado = $_POST['name'];
        $muni = $_POST['mun'];

        $sucursales = DB::table('offices')
        ->where('estado', '=', $estado)
        ->get();

         return view('sucursales.form.resultado', compact('sucursales', 'muni'));
           }



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Crear solicitud



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sucursal = DB::table('offices')
        ->where('nombre_suc', '=', $id)
        ->get();
        //El id de la sucursal
        $idSuc = $sucursal[0]->id;

        $users = DB::table('users')
        ->leftJoin('controls', 'users.id', '=', 'controls.idUser')
        ->leftJoin('offices', 'controls.idSucursal', '=', 'offices.id')
        ->join('roles', 'controls.idRol', '=', 'roles.id')
        ->where('controls.idSucursal', '=', $idSuc)
        ->get();


        return view('sucursales.show', compact('sucursal', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
