<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Online extends Model
{
    protected $fillable = [
            'nombres',
            'apellidoP',
            'apellidoM',
            'email',
            'celular',
            'comentarios',
            'fecha_ini',
            'fecha_mod'
    ];
}
