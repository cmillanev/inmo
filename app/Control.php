<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    //
    protected $fillable =  ['idSucursal', 'idUser', 'idRol'];
}
