<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewFile extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Bienvedido a Pólizas de Rentas';
    public $msg;
    public $email;
   
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg, $email)
    {
        //
        $this->msg = $msg;
        $this->email = $email;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.nuevaSolicitud');
    }
}
