<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Documentacion extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Solicitud en documentación';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg, $email)
    {
        $this->msg = $msg;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.documentacion');
    }
}
