<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contacto extends Mailable
{
    use Queueable, SerializesModels;


    public $subject = 'Nuevo mensaje';
    public $msg;
    public $email;
    public $email_suc;
    public $nombre;
    public $celular;
    public $hoy;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg, $email, $email_suc, $nombre, $celular, $hoy)
    {
        //
        $this->msg = $msg;
        $this->email = $email;
        $this->email_suc = $email_suc;
        $this->nombre = $nombre;
        $this->celular = $celular;
        $this->hoy = $hoy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //Envia correo a sucursal o abogado con el
        return $this->view('maileclipse::templates.contactowp');
    }
}
