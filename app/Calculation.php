<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calculation extends Model
{
    //
    protected $fillable =  ['limInferior', 'limSuperior', 'cuotaFija', 'porciento'];
}
