<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{

    public $timestamps = false;
    //
    protected $fillable = [
            'nombre_suc',
            'email_suc',
            'codigo',
            'calle',
            'numInt',
            'numExt',
            'colonia',
            'municipio',
            'estado',
            'cp',
            'telefono_suc',
            'lat',
            'lng',
            'img_ciudad',
            'img_suc',
            'descripcion_suc',
            'fecha_ini',
            'fecha_mod'
    ];
}
